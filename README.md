# Qubo
The Qubo repository is a ROS2 Humble package that contains only everything needed for Qubo to run autonomously, meant to be built in a ROS2 Humble workspace on Qubo itself.
See https://code.umd.edu/robotics-at-maryland/ram/-/wikis/yearly/2024-2025/software-projects/codebase-documentation

## Setup
Setup ROS and Create a ROS2 workspace:
```sh
source /opt/ros/humble/setup.bash
mkdir -p ros2_ws/src
cd ros2_ws/src
```

Install dependencies:
```sh
apt install libi2c-dev ros-humble-avt-vimba-camera ros-humble-robot-localization ros-humble-behaviortree-cpp-v3
```
and
```sh
git clone --recurse-submodules https://github.com/stereolabs/zed-ros2-wrapper.git
git clone --recurse-submodules https://code.umd.edu/robotics-at-maryland/packages/dvl-a50.git
git clone https://code.umd.edu/robotics-at-maryland/packages/dvl_msgs.git
git clone https://code.umd.edu/robotics-at-maryland/packages/qubo_interfaces.git
git clone https://code.umd.edu/robotics-at-maryland/packages/vectornav.git
cd vectornav
git checkout ros2
cd ../..
sudo apt update
rosdep install --from-paths src --ignore-src -r -y
```
If rosdep is missing you can install it with
```sh
apt-get install python3-rosdep python3-rosinstall-generator python3-vcstool python3-rosinstall build-essential
```

Build the code: (from the ros2_ws directory)
```sh
colcon build --symlink-install --cmake-args=-DCMAKE_BUILD_TYPE=Release
source install/local_setup.bash
```

For running simulation see https://code.umd.edu/robotics-at-maryland/qubo_sim