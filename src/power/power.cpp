#include <cmath>
#include <chrono>

// ROS Includes
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/battery_state.hpp"

#include "qubo/ADS7828.h"
#include "qubo/power.h"

/* this will be a publisher, basic idea of operation */

/* constantly loop through all the channels, publish each message
   Type 1 (Ch0/1): Input current and voltage -- this is the battery life
   Type 2 (Ch2/3): 12V Current and Voltage -- this goes to the DVL and 
                                              the thrusters
   Type 3 (Ch4): 5V Current
   Type 4 (Ch5): 3.3 Current


   Actually I think this is everything that has to be done, handle all the
   conversions and here and we should be good
*/

PowerPublisher::PowerPublisher(ADS7828 &ads)
    : Node("power_publisher"), power_chip(ads) {
    publisher = this->create_publisher<BatteryState>("power_report", 10);
    timer = this->create_wall_timer(
        std::chrono::milliseconds(15),
        std::bind(&PowerPublisher::publish_all, this));
}

void PowerPublisher::publish_all() {
    std::cout << "*************BATTERY" << std::endl;
    PowerPublisher::prepare_battery_info();
    std::cout << "*************12V CHANNEL" << std::endl;
    PowerPublisher::prepare_12v_info();
    std::cout << "**************5V CHANNEL" << std::endl;
    PowerPublisher::prepare_5v_info();
    std::cout << "**************3.3V CHANNEL" << std::endl;
    PowerPublisher::prepare_3_3v_info();
}

//TODO: write a function here to convert 12-bit precision to float
void PowerPublisher::prepare_battery_info() {
    float current = power_chip.read_channel_current(0);
	float voltage = power_chip.read_channel_voltage(1);
    auto msg = sensor_msgs::msg::BatteryState();

    fill_simple_msg(msg, "battery", current, voltage);

    publisher->publish(msg);
}

void PowerPublisher::prepare_12v_info() {
    float current = power_chip.read_channel_current(2);
	float voltage = power_chip.read_channel_voltage(3);
    std::cout << "voltage after reading in power.cpp: " << voltage << std::endl;
    auto msg = sensor_msgs::msg::BatteryState();

    fill_simple_msg(msg, std::string("12V channel"), current, voltage);

    publisher->publish(msg);
}

void PowerPublisher::prepare_5v_info() {
    float current = power_chip.read_channel_current(4);
    auto msg = sensor_msgs::msg::BatteryState();

    fill_simple_msg(msg, "5V channel", current, 0.0f);
    
    publisher->publish(msg);
}

void PowerPublisher::prepare_3_3v_info() {
    float current = power_chip.read_channel_current(5);
    auto msg = sensor_msgs::msg::BatteryState();

    fill_simple_msg(msg, "3.3V channel", current, 0.0f);

    publisher->publish(msg);
}

void PowerPublisher::fill_simple_msg(BatteryState &msg, std::string loc,
                                     float current, float voltage) {
	std::cout << "voltage when filling in message: " << voltage << std::endl;
    msg.voltage = voltage;
    msg.temperature = NAN;
    msg.current = current; //need to convert to float here
    msg.charge = NAN;
    msg.capacity = NAN;
    msg.design_capacity = NAN;
    msg.percentage = NAN;
    msg.power_supply_status = 2;
    msg.power_supply_health = 0;
    msg.power_supply_technology = 0;
    msg.present = true;
    msg.cell_voltage = {};
    msg.cell_temperature = {};
    msg.location = loc;
    msg.serial_number = "";
}

int main(int argc, char *argv[]) {
    ADS7828 &ads = ADS7828::get_instance();

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<PowerPublisher>(ads));
    rclcpp::shutdown();

    return 0;
}
