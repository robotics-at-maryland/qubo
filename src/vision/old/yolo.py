#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Vector3
from vision_msgs.msg import BoundingBox2D, BoundingBox2DArray, Pose2D, Point2D
from sensor_msgs.msg import Image
from rclpy.qos import qos_profile_sensor_data
qos_pf = qos_profile_sensor_data
import cv2
from cv_bridge import CvBridge


import numpy as np


from ultralytics import YOLO

class Yolo(Node):
    def __init__(self, show=False):
        super().__init__("yolo")
        self.subscription = self.create_subscription(
            Image,
            '/zed_img',
            self.listener_callback,
            qos_profile=qos_pf
            # queue_size=1
            )
        self.bridge = CvBridge()
        self.boxes_pub = self.create_publisher(
            BoundingBox2DArray,
            '/detections',
            1
        )
        self.gate_pub = self.create_publisher(
            Vector3,
            'gate_bbox',
            1
        )
        self.buoy_pub = self.create_publisher(
            Vector3,
            'buoy_bbox',
            1
        )
        self.opened_pub = self.create_publisher(
            Vector3,
            'opened_bbox',
            1
        )
        self.iris_pub = self.create_publisher(
            Vector3,
            'iris_bbox',
            1
        )

        self.classes = {
            0 : 'Buoy',
            1 : 'Buoy_A1',
            2 : 'Buoy_A2',
            3 : 'Buoy_E1',
            4 : 'Buoy_E2',
            5 : 'Closed',
            6 : 'Closed_Iris',
            7 : 'Gate',
            8 : 'Gate_A',
            9 : 'Gate_E',
            10 : 'Opened',
            11 : 'Opened_Iris',
            12 : 'Torpedo_1',
            13 : 'Torpedo_2'
        }

        self.model = YOLO('/home/josh/ros_ws/src/qubo/vision/ai/best4.pt')
        print("Done Loading")

    def listener_callback(self, msg):
        img = self.bridge.imgmsg_to_cv2(msg)
        square_img = cv2.resize(img, (800, 800))
        results = self.model(square_img)
        bboxes = BoundingBox2DArray()
        for result in results:
            top_conf = [False, False, False, False]
            for box in result.boxes:
                box_class = self.classes[box.data[0][5].item()]
                print(box_class)

                # RESIZE BACK TO 1280X720
                xscale = 1280/800
                yscale = 720/800

                
                corner1 = (int(box.xyxy[0][0].item()*xscale), int(box.xyxy[0][1].item()*yscale))
                corner2 = (int(box.xyxy[0][2].item()*xscale), int(box.xyxy[0][3].item()*yscale))
                cv2.rectangle(img, corner1, corner2, (0, 255, 255), 3)
                cv2.putText(img, box_class, corner1, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 1, cv2.LINE_AA)
                

                s_x = box.xywh[0][0].item() * xscale
                s_y = box.xywh[0][1].item() * yscale
                s_w = box.xywh[0][2].item() * xscale
                s_h = box.xywh[0][3].item() * yscale
                bbox = BoundingBox2D(
                center = Pose2D(
                    position = Point2D(
                        x = s_x,
                        y = s_y
                    ),
                    theta = 0.0
                ),
                size_x = s_w,
                size_y = s_h
                )
                bboxes.boxes.append(bbox)

                # Confidence vals for all tasks acquired, so publish the desired info 
                # to the desired topic
                if box_class == "Opened_Iris" and not top_conf[0]:
                    vbox = Vector3(
                        x = s_x,
                        y = s_y,
                        z = s_h
                    )
                    self.iris_pub.publish(vbox)
                    top_conf[0] = True
                else:
                    vbox = Vector3(
                        x = s_x,
                        y = s_y,
                        z = s_w
                    )
                    if box_class == "Gate" and not top_conf[1]:
                        self.gate_pub.publish(vbox)
                        top_conf[1] = True
                    elif box_class == "Buoy" and not top_conf[2]:
                        self.buoy_pub.publish(vbox)
                        top_conf[2] = True
                    elif box_class == "Opened" and not top_conf[3]:
                        self.opened_pub.publish(vbox)
                        top_conf[3] = True
                    

            cv2.imshow('detections', img)
            cv2.waitKey(30)
        
        self.boxes_pub.publish(bboxes)



if __name__ == "__main__":
    rclpy.init(args=None)

    yolo = Yolo(show=False)
    rclpy.spin(yolo)
