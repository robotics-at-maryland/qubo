#!/usr/bin/env python3
import numpy as np
import cv2
from cv_bridge import CvBridge
import matplotlib.pyplot as plt

import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
qos_pf = qos_profile_sensor_data

from sensor_msgs.msg import Image
from geometry_msgs.msg import Vector3

def draw_lines(img, lines, color = (0,255,0)):
    first = True
    for r_theta in lines:
        arr = np.array(r_theta[0], dtype=np.float64)
        r, theta = arr
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*r
        y0 = b*r
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        if first:
            cv2.line(img, (x1, y1), (x2, y2), color, 2)
            first = False
        else:
            cv2.line(img, (x1, y1), (x2, y2), (255,255,255), 2)
    return img

def check_pipe(left_line, right_line, img, draw_img = None, angle_diff_thresh = 0.15, hori_fov = 90):
    if left_line is None or right_line is None:
        return None
    
    
    l_r, l_theta = left_line[0]
    r_r, r_theta = right_line[0]
    if np.abs(l_theta - r_theta) % 3.14 > angle_diff_thresh:
        return None
    
    mid_row = img.shape[0]//2
    print(img.shape)

    a_l = np.cos(l_theta)
    b_l = np.sin(l_theta)
    x0_l = a_l*l_r
    y0_l = b_l*l_r

    a_r = np.cos(r_theta)
    b_r = np.sin(r_theta)
    x0_r = a_r*r_r
    y0_r = b_r*r_r

    mid_x_l = x0_l + ((mid_row - y0_l) * -np.tan(l_theta))#+ np.abs((mid_row - y0_l) * b_l)
    mid_x_r = x0_r + ((mid_row - y0_r) * -np.tan(r_theta))

    depth = np.median(img[mid_row, (int)(mid_x_l): (int)(mid_x_r) + 1])
    print("depth", depth)

    cv2.circle(draw_img, ((int)(mid_x_l), mid_row), 10, (0,255,0), -1)
    cv2.circle(draw_img, ((int)(mid_x_r), mid_row), 10, (0,255,0), -1)

    xAngle = ((((mid_x_l + mid_x_r) / 2) - (img.shape[1]/2)) / img.shape[1]) * hori_fov

    return (xAngle, depth)

def vertEdgeDetect(img, blur=10, nms_length = 3, thresh = 50, min_length = 100):

    blurred_img = cv2.blur(img, (blur,blur))
    kernel1 = np.array([[-1, 0, 1]])
    hori_img_grad = cv2.filter2D(blurred_img, ddepth=-1, kernel=kernel1)
    nms_img = np.zeros((hori_img_grad.shape[0],hori_img_grad.shape[1],nms_length))
    widthNMS = hori_img_grad.shape[1] - 2 * (nms_length//2)
    for i in range(nms_length):
        x = i - nms_length//2
        nms_img[:,(nms_length//2) : (nms_length//2) + widthNMS, i] = hori_img_grad[:,x + (nms_length//2) : x + (nms_length//2) + widthNMS]
    
    max_arg = np.argmax(nms_img, axis=2)
    bool_edges_right = np.logical_and((max_arg == nms_length//2), (hori_img_grad > thresh)).astype(np.uint8)

    min_arg = np.argmin(nms_img, axis=2)
    bool_edges_left = np.logical_and((min_arg == nms_length//2), (hori_img_grad < -thresh)).astype(np.uint8)


    right_lines = cv2.HoughLines(bool_edges_right,2,np.pi/90, min_length)
    left_lines = cv2.HoughLines(bool_edges_left,2,np.pi/90, min_length)
    return left_lines, right_lines

class PipePublisher(Node):

    def __init__(self, show=False):
        super().__init__('gatepub')
        self.subscription = self.create_subscription(
            Image,
            '/zed_depth',
            self.listener_callback,
            qos_profile=qos_pf)
        self.pipe_pub = self.create_publisher(
            Vector3,
            '/pipe_pos',
            1)
        self.subscription  # prevent unused variable warning
        self.bridge = CvBridge()
        self.i = 0
        self.show = show

    def listener_callback(self, msg):
        img = self.bridge.imgmsg_to_cv2(msg)

        img = np.nan_to_num(img, nan=0, posinf=0, neginf=0)#img = remove_nan_smooth(img)

        left_lines, right_lines = vertEdgeDetect(img)


        right_line = left_line = None

        if self.show:
            draw_img = cv2.cvtColor((255 * (img) / np.max(img)).astype(np.uint8), cv2.COLOR_GRAY2BGR)

        draw_img = None
        if right_lines is not None:
            if self.show:
                draw_img = draw_lines(draw_img, right_lines, color=(0,0,255))
            right_line = right_lines[0]
        if left_lines is not None:
            if self.show:
                draw_img = draw_lines(draw_img, left_lines, color=(255,0,0))
            left_line = left_lines[0]

        pipe = check_pipe(left_line, right_line, img, draw_img = draw_img)

        pipe_msg = Vector3()
        if pipe is not None:
            print("Pipe found, xAngleRel = ", pipe[0], " depth = ", pipe[1])
            pipe_msg.x = 1.0
            pipe_msg.y = (float)(pipe[0])
            pipe_msg.z = (float)(pipe[1])
        else:
            pipe_msg.x = -1.0


        self.pipe_pub.publish(pipe_msg)
        
        if self.show:
            cv2.imshow("pipe img", draw_img)
            cv2.waitKey(30)




if __name__ == "__main__":
    rclpy.init(args=None)

    img_num = 0
    pipePub = PipePublisher(show=False)
    rclpy.spin(pipePub)
