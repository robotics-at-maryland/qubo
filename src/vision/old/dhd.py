#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Image
from rclpy.qos import qos_profile_sensor_data
qos_pf = qos_profile_sensor_data

import cv2
from cv_bridge import CvBridge

class DHD(Node):
    def __init__(self, show=False):
        super().__init__("dhd")
        self.cam_sub = self.create_subscription(
            Image,
            '/mako/image',
            self.locate_dhd,
            qos_profile=qos_pf
        )
        self.bridge = CvBridge()

        self.dhd_pub = self.create_publisher(
            Vector3,
            'dhd',
            1
        )

    def locate_dhd(self, imgmsg):
        img = self.bridge.imgmsg_to_cv2(imgmsg)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gray, 250, 255, 0)

        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (29, 29))
        open = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
        close = cv2.morphologyEx(open, cv2.MORPH_CLOSE, kernel)

        contours, hierarchy = cv2.findContours(close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        big_contour = max(contours, key=cv2.contourArea)
        area = cv2.contourArea(big_contour)

        epsilon = 0.023*cv2.arcLength(big_contour,True)
        oapprox = cv2.approxPolyDP(big_contour,epsilon,True)
        approx = oapprox.reshape(len(oapprox), 2).tolist()
        vertices = len(approx)
        
        x, y, w, h = cv2.boundingRect(big_contour)
        asp_rat = float(w) / h

        is_convex = cv2.isContourConvex(oapprox)

        if (area > 70000 and area < 100000 and
            vertices == 8 and
            asp_rat > 0.8 and asp_rat < 1.2 and
            is_convex):

            cv2.drawContours(img, big_contour, -1, (0,255,0), 3)
            for point in approx:
                cv2.circle(img, point, radius=8, color=(0, 0, 255), thickness=-1)

            center, radius = cv2.minEnclosingCircle(big_contour)
            dhd_center = Vector3(x=center[0], y=center[1], z=1)
            self.dhd_pub.publish(dhd_center)
        else:
            self.dhd_pub.publish(Vector3(x=0, y=0, z=0))
        
        cv2.imshow('dhd_detection', img)
        cv2.waitKey(30)



if __name__ == "__main__":
    rclpy.init(args=None)

    dhd = DHD()
    rclpy.spin(dhd)
