#!/usr/bin/python3
import torchvision.transforms.transforms
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge
from pose_detector import UNet
import torch
from torchvision.transforms.transforms import ToTensor, Compose

class ImageDetection(Node):
    def __init__(self):
        super().__init__("image_detection_node")
        self.loaded_model = UNet()
        checkpoint = torch.load("./pose256V1.pth")
        self.loaded_model.load_state_dict(checkpoint['model_state_dict'])
        self.loaded_model.eval()
        self.create_subscription(
            Image,
            "/zed/zed_node/rgb/image_rect_color",
            self.run_image_detection,
            5
        )
        self.bridge = CvBridge()
        
    def run_image_detection(self, msg : Image):
        cv2_img = self.bridge.imgmsg_to_cv2(msg)
        # RUN YOLO MODEL AND SEGMENT
        cv2_img = cv2.resize(cv2_img, (341, 256))
        self.run_pose_detection(cv2_img)
        # PUBLISH TO APPROPRIATE TOPICS FOR YOLO

    def run_pose_detection(self, img):
        with torch.no_grad():
            transform = Compose([ToTensor()])
            tensor_img = transform(img)
            output = self.loaded_model(tensor_img)
        # PUBLISH TO APPROPRIATE TOPICS
        pass


def main(args=None):
    rclpy.init(args=args)
    image_detection_node = ImageDetection()
    rclpy.spin(image_detection_node)

if __name__ == "__main__":
    main()