import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage
from tqdm import tqdm
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torch import optim

class UNet(nn.Module):
  def __init__(self):
    super(UNet, self).__init__()
    self.n_channels = 3
    smallest_channels = 64
    self.in_conv = UNet.double_conv(self.n_channels, smallest_channels)
    self.down1 = UNet.down(smallest_channels, smallest_channels*2)
    self.down2 = UNet.down(smallest_channels*2, smallest_channels*4)
    self.down3 = UNet.down(smallest_channels*4, smallest_channels*4)

    # self.out_conv = nn.Sequential(
    #     nn.Conv2d(in_channels=smallest_channels*4, out_channels=1, kernel_size=1),
    #     nn.Sigmoid()
    # )

    self.classifier = nn.Sequential(
        nn.Flatten(),
        nn.Linear(in_features=344064, out_features=12, bias=True),
        nn.ReLU(),
        nn.Linear(in_features=12, out_features=8, bias=True),
        nn.ReLU(),
        nn.Linear(in_features=8, out_features=3, bias=True),
        nn.ReLU(), 
    )
  # 3x3 conv --> batch normalize --> ReLU (twice)
  class double_conv(nn.Module):
    def __init__(self, in_channels, out_channels):
      super().__init__()

      self.double_conv = nn.Sequential(
          nn.Conv2d(in_channels, out_channels, kernel_size = 3, padding=1, bias=False),
          nn.BatchNorm2d(out_channels),
          nn.ReLU(inplace=True),
          nn.Conv2d(out_channels, out_channels, kernel_size = 3, padding=1, bias=False),
          nn.BatchNorm2d(out_channels),
          nn.ReLU(inplace=True)
      )

    def forward(self, x):
      return self.double_conv(x)

  # 2x2 maxpool --> double_conv
  class down(nn.Module):
    def __init__(self, in_channels, out_channels):
      super().__init__()

      self.down = nn.Sequential(
        nn.MaxPool2d(2),
        UNet.double_conv(in_channels, out_channels)
      )

    def forward(self, x):
      return self.down(x)

  def forward(self, input):
    x_1 = self.in_conv(input)
    # print(f'x_1:{x_1.shape}')
    x_2 = self.down1(x_1)
    # print(f'x_2:{x_2.shape}')
    x_3 = self.down2(x_2)
    # print(f'x_3:{x_3.shape}')
    x_4 = self.down3(x_3)
    # print(f'x_4:{x_4.shape}')

    logits = self.classifier(x_4)

    return logits