#!/usr/bin/env python

# Nate Renegar
# R@M 2019
# naterenegar@gmail.com

import rospy
from vision.srv import GateDistance

def getGateDistance():
	rospy.wait_for_service('gate_distance')
	try:
		gateDistance = rospy.ServiceProxy('gate_distance', GateDistance)
		response = gateDistance()	
		print(response.distance)
		print(response.theta)
		print(response.data_valid)
	except rospy.ServiceException, e:
		print "Service call failed: %s"%e

if __name__ == "__main__":
	while(True):
		getGateDistance()

