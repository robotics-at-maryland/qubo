#!/usr/bin/env python

# Nate Renegar
# R@M 2019
# naterenegar@gmail.com

from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
from vision.srv import GateDistance
import rospy
import cv2
import numpy as np
import math
import roslib
roslib.load_manifest('vision')

class vision_server:

	def __init__(self):
		self.mako_sub = rospy.Subscriber("mako_feed", Image, self.mako_callback)
		self.hull_sub = rospy.Subscriber("hull_feed", Image, self.hull_callback)
		self.bridge   = CvBridge()
		self.mako_image = None
		self.gate_distance_s = rospy.Service('gate_distance', GateDistance, self.calculate_gate)

	def mako_callback(self, data):	
		try:
			self.mako_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
			print(type(self.mako_image))
		except CvBridgeError as e:
			print(e)	

	def hull_callback(self, data):
		try:
			self.hull_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
		except CvBridgeError as e:
			print(e)	

	# request argument not used, but is necessary
	def calculate_gate(self, req):
		gray = cv2.cvtColor(self.mako_image, cv2.COLOR_BGR2GRAY)
		edges = cv2.Canny(gray, 40, 200)
		lines = cv2.HoughLinesP(edges, 1, np.pi/180, 60, None, 50, 10)

		verts = [x for x in lines if self.is_vertical(x)]
		means = [(l[0][0] + l[0][2]) / 2 for l in verts]

		mean = lambda x: sum(x) / len(x)
		avg = mean(means)
		left = mean([x for x in means if x < avg])
		right = mean([x for x in means if x >= avg])
		if lines is not None:
			for i in range(0, len(lines)):
				x1, y1, x2, y2 = lines[i][0]

#				angle = math.atan2(y2 - y1, x2 - x1) * 180.0 / np.pi
#				if (angle < 115 and angle > 65) or (angle > -115 and angle < -65):
#					cv2.line(self.mako_image, (x1, y1), (x2, y2), (255, 0, 0), 3)
#			cv2.line(self.mako_image, (left, 0), (left, 1000), (0, 0, 255), 3)
#			cv2.line(self.mako_image, (right, 0), (right, 1000), (0, 0, 255), 3)
#			cv2.line(self.mako_image, ((left + right)/2, 0), ((left + right)/2, 1000), (0, 255, 0), 3)

			# this calculates distance assuming gate is perpendicular to center of robots FOV
			gate_length_m = 3.048;
			image_x_dim = self.mako_image.shape[1]

			# Number of pixels spanning the gate at a known distance from the camera
			# This is a placeholder. The actual value will be determined later
			focal_length_ref = int(0.5 * image_x_dim)
			focal_length_obj = right - left 
			focal_length_mid = int(((left + left) / 2) - (image_x_dim / 2))
			distance_ref = 1.0; 
			distance_obj = distance_ref * focal_length_ref / focal_length_obj;
			distance_mid = distance_ref * focal_length_ref / focal_length_mid;	
			theta = math.atan2(distance_mid, distance_obj)

#			cv2.imshow('lines', self.mako_image)
			return distance_obj, theta, True
		else:
			return -1, -1, False

	def is_vertical(self, x):
		x1, y1, x2, y2 = x[0]
		angle = math.atan2(y2 - y1, x2 - x1) * 180.0 / np.pi
		if (angle < 115 and angle > 65) or (angle > -115 and angle < -65):
			return True
		return False

def vision_server_startup():
	vs = vision_server();
	rospy.init_node('gate_distance_server')
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
	cv2.destroyAllWindows()

if __name__ == "__main__":
	vision_server_startup()
