import numpy as np
import cv2 

hull_cam = cv2.VideoCapture(0)

if hull_cam.isOpened() == False:
	hull_cam.open()


while(True):
	ret, frame = hull_cam.read()
	
	cv2.imshow('hull camera feed', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

hull_cam.release()
cv2.destroyAllWindows()
