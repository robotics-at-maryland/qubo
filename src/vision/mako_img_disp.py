#!/usr/bin/python3
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge

class MakoImageDisplayer(Node):
    def __init__(self):
        super().__init__("mako_image_displayer_node")
        self.create_subscription(
            Image,
            "/mako/image",
            self.show_image,
            5
        )
        self.bridge = CvBridge()
    
    def show_image(self, msg : Image):
        cv2_img = self.bridge.imgmsg_to_cv2(msg)
        cv2.imshow("POV: Mexico", cv2_img)
        cv2.waitKey(30)

def main():
    rclpy.init()
    mako_image_displayer_node = MakoImageDisplayer()
    rclpy.spin(mako_image_displayer_node)

if __name__ == "__main__":
    main()