/**  
  *  Nathaniel Renegar
  *  R@M 2019
  *  naterengar@gmail.com
  **/


#ifndef HULL_NODE_H
#define HULL_NODE_H

//opencv includes
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <cv_bridge/cv_bridge.h>

