#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Wrench, Vector3 
from std_msgs.msg import Float32, Int32
import math
import numpy as np
import time


class AutoNode(Node):

    def __init__(self, name, thrust_channel='desired_thrust', z_channel = 'z_control', yaw_channel = 'yaw_control', rightWall=True):
        super().__init__(name)
        # Publish thrust vector
        self.thrust_publisher_ = self.create_publisher(Wrench, thrust_channel, 1)
        # Publish desired yaw value to yaw_pid.py
        self.des_yaw_publisher_ = self.create_publisher(Float32, 'desired_yaw', 1)
        # Publish desired depth value to depth_pid.py
        self.des_depth_publisher_ = self.create_publisher(Float32, 'desired_depth', 1)
        self.torpedo_publisher_ = self.create_publisher(Int32, 'torpedo_trigger', 1)
        self.led_publisher_ = self.create_publisher(Int32, 'led_mode', 1)
        # Get thrust value for thrust vector to get to desired yaw
        # From yaw_pid.py
        self.subscription_yaw_control = self.create_subscription(
            Float32,
            yaw_channel,
            self.yaw_control_callback,
            2)

        # Get thrust value for thrust vector to get to desired depth
        # From depth_pid.py
        self.subscription_depth_control = self.create_subscription(
            Float32,
            z_channel,
            self.depth_control_callback,
            2)
        
        # Get [roll, pitch, yaw] from IMU
        # From imu_eul_pub.py
        self.yaw_subscription = self.create_subscription(
            Vector3,
            'rpy',
            self.curr_yaw_callback,
            1)

        # Get current depth from pressure sensor
        # depthPub.py
        self.depth_subscription = self.create_subscription(
            Float32,
            'depth',
            self.curr_depth_callback,
            1)

        # Get 
        self.gate_subscription = self.create_subscription(
            Vector3,
            '/gate_bbox',
            self.gate_callback,
            1)
        self.buoy_subscription = self.create_subscription(
            Vector3,
            '/buoy_bbox',
            self.buoy_callback,
            1)
        self.opened_subscription = self.create_subscription(
            Vector3,
            '/opened_bbox',
            self.opened_callback,
            1)
        self.iris_subscription = self.create_subscription(
            Vector3,
            '/iris_bbox',
            self.iris_callback,
            1)
        self.wall_subscription = self.create_subscription(
            Vector3,
            '/wall_pos',
            self.wall_callback,
            1)

        self.oct_subscription = self.create_subscription(
            Vector3,
            'dhd',
            self.oct_callback,
            1)

        timer_period = 0.05  # seconds
        self.timer = self.create_timer(timer_period, self.main_callback)
        # Max thrust value for x, y, z, roll, pitch, yaw respectively
        self.thrust_clip = np.array([1.4, 1.4, 1.6, 0.8, 0.8, 1.0])
        # Set vector for desired thrust, as x, y, z, roll, pitch, yaw
        self.desired_thrust = np.zeros((6))
        # For torpedo?
        self.left_fired = False

        # Initialize behavior tree variables
        self.state = "init" #states are init, gate_center, through_gate, pipe_center, rotate_pipe
        self.controlledZ = 0
        self.rightWall = rightWall
        self.wallNegate = -1.0
        self.wall_dist = 4000
        if self.rightWall:
            self.wallNegate = 1.0
        self.controlledYaw = 0
        self.start_yaw = -80.0
        self.desired_yaw = self.start_yaw 
        self.desired_depth = 0
        self.desired_torpedo_depth = -2.3
        self.desired_gate_yaw = self.desired_yaw
        self.desired_buoy_yaw = self.desired_yaw
        self.desired_opened_yaw = self.desired_yaw
        self.desired_iris_yaw = self.desired_yaw
        self.firstCB = True
        self.currYaw = 0
        self.currDepth = 0
        self.curr_gate_width = 0
        self.curr_opened_width = 400
        self.curr_iris_width = 200
        self.curr_buoy_width = 0
        self.octFound = False
        self.octTrack = False
        self.octX = 1280/2
        self.octY = 1024/2

    def pub_desired_yaw(self):
        des_yaw_msg = Float32()
        des_yaw_msg.data = (float)(self.desired_yaw)
        self.des_yaw_publisher_.publish(des_yaw_msg)

    def pub_desired_depth(self):
        des_depth_msg = Float32()
        des_depth_msg.data = (float)(self.desired_depth)
        self.des_depth_publisher_.publish(des_depth_msg)
    
    def reset_state_time(self):
        self.startStateTime = time.time()
        self.currStateTime = (time.time() - self.startStateTime)

    def main_callback(self):
        self.desired_thrust = np.zeros((6)) # Reset thrust

        if self.firstCB:
            self.firstCB = False
            self.startTime = time.time()
            self.startStateTime = time.time()

        self.currTime = (time.time() - self.startTime)
        self.currStateTime = (time.time() - self.startStateTime)

        # Set initial z and yaw values
        self.desired_thrust[2] = self.controlledZ
        self.desired_thrust[5] = self.controlledYaw

        # Sequence: 
        if self.state == "init":
            if self.currStateTime > 30:
                self.state = "down"
                self.desired_depth =  -1.2
                self.reset_state_time()
            elif self.currStateTime > 20:
                self.desired_thrust = self.desired_thrust * 0
                self.set_led(1)
            elif self.currStateTime > 10:
                self.set_led(3)
                self.start_yaw = self.currYaw
                self.desired_thrust = self.desired_thrust * 0
            else:
                self.set_led(6)
                self.start_yaw = self.currYaw
                self.desired_thrust = self.desired_thrust * 0
        elif self.state == "down":
            self.desired_yaw = self.start_yaw
            if self.currStateTime > 8:
                self.state = "forward"
                self.desired_gate_yaw = self.start_yaw
                self.curr_gate_width = 0
                self.reset_state_time()
        elif self.state == "forward":
            self.desired_yaw = self.desired_gate_yaw
            self.desired_thrust[1] = 1.2
            if self.currStateTime > 16:
                self.state = "gate_center"
                self.reset_state_time()
        elif self.state == "gate_center":
            self.desired_yaw = self.desired_gate_yaw
            
            self.desired_thrust[1] = (800 - self.curr_gate_width ) / 200 ##
            if self.currStateTime > 18:
                self.save_gate_yaw = self.desired_gate_yaw
                self.state = "forward2"
                self.reset_state_time()
        elif self.state == "forward2":
            self.desired_thrust[1] = 1.2
            if self.currStateTime > 18:
                self.state = "stop"
                self.desired_depth =  -1.7
                self.reset_state_time()
        elif self.state == "stop":
            self.desired_thrust[1] = -1.0
            if self.currStateTime > 0.5:
                self.state = "spin"
                self.reset_state_time()
                
        elif self.state == "spin":
            
            if self.currStateTime >= 24:
                self.desired_yaw = self.start_yaw
                self.desired_buoy_yaw = self.start_yaw
                self.curr_buoy_width = 0
                self.state = "forward3"
                self.reset_state_time()
            else:
                self.desired_yaw = self.save_gate_yaw + 120 * (1 + (self.currStateTime // 4))
        elif self.state == "forward3":
            self.desired_thrust[1] = 1.2
            self.curr_gate_width = 0
            self.desired_yaw = self.desired_buoy_yaw
            if self.currStateTime > 6:
                self.state = "buoy_center"
                self.reset_state_time()
        elif self.state == "buoy_center":
            self.desired_yaw = self.desired_buoy_yaw
            
            self.desired_thrust[1] = (700 - self.curr_buoy_width ) / 200 ##
            if self.currStateTime > 18:
                self.save_buoy_yaw = self.desired_buoy_yaw
                self.state = "bump_forward"
                self.reset_state_time()
        elif self.state == "bump_forward":
            self.desired_thrust[1] = 1.2
            if self.currStateTime > 10:
                self.state = "bump_backward"
                self.reset_state_time()
        elif self.state == "bump_backward":
            self.desired_thrust[1] = -1.2
            if self.currStateTime > 10:
                self.state = "stop2"
                self.reset_state_time()
        elif self.state == "stop2":
            self.desired_thrust[1] = 1.0
            if self.currStateTime > 0.5:
                self.state = "side"
                self.reset_state_time()
        elif self.state == "side":
            self.desired_thrust[0] = -1.2 * self.wallNegate
            if self.currStateTime > 10:
                self.state = "forward4"
                self.reset_state_time()
                self.desired_opened_yaw = self.desired_yaw
        elif self.state == "forward4":
            self.desired_yaw = self.desired_opened_yaw
            self.desired_iris_yaw = self.desired_opened_yaw
            self.desired_thrust[1] = 1.2
            if self.currStateTime > 15:
                self.state = "opened_center"
                self.desired_torpedo_depth = -2.2
                self.reset_state_time()
        elif self.state == "opened_center":
            self.desired_yaw = self.desired_opened_yaw
            self.desired_depth = self.desired_torpedo_depth
            self.desired_thrust[1] = (700 - self.curr_opened_width ) / 200.0 ##
            if self.currStateTime > 18:
                self.state = "fire"
                self.reset_state_time()
        elif self.state == "fire":
            self.desired_yaw = self.desired_iris_yaw
            self.desired_depth = self.desired_torpedo_depth
            self.desired_thrust[1] = (230 - self.curr_iris_width ) / 100.0 ##
            if self.currStateTime > 34:
                self.state = "turn"
                self.center_torpedo()
                self.reset_state_time()
                self.desired_yaw = self.desired_iris_yaw - (90 * self.wallNegate)
                self.desired_depth = -1.2
            elif self.currStateTime > 32:
                self.fire_right()
            elif self.currStateTime > 22:
                self.left_fired = True
            elif self.currStateTime > 20: 
                self.fire_left()
            else:
                pass
        elif self.state == "turn":
            if self.currStateTime > 6:
                self.state = "forwardWall"
                self.reset_state_time()
        elif self.state == "forwardWall":
            self.desired_depth = -0.6
            self.desired_thrust[1] = 1.2
            if self.currStateTime > 20:
                self.desired_wall_yaw = self.desired_yaw
                self.wall_dist = 4000
                self.state = "stopWall"
                self.reset_state_time()
        elif self.state == "stopWall":
            self.desired_thrust[1] = (self.wall_dist - 1900) / 300.0
            if self.currStateTime > 20:
                self.desired_yaw = self.desired_wall_yaw
                self.state = "followWall"
                self.reset_state_time()
        elif self.state == "followWall":
            self.desired_thrust[1] = (self.wall_dist - 1900) / 300.0
            self.desired_thrust[0] = -1.5 * self.wallNegate
            self.desired_yaw = self.desired_wall_yaw
            if self.currStateTime > 150 or self.octFound:
                self.desired_yaw = self.desired_wall_yaw
                self.state = "centerOct"
                self.reset_state_time()
        elif self.state == "centerOct":
            if self.octTrack:
                self.desired_thrust[1] = ((1024/2) - self.octY) / 400
                self.desired_thrust[0] = (self.octX - (1280/2)) / 400
            else:
                self.desired_thrust[1] = 0
                self.desired_thrust[0] = 0

            if self.currStateTime > 15:
                self.state = "surface"
                self.reset_state_time()
        elif self.state == "surface":
            self.desired_thrust[1] = 0
            self.desired_thrust[0] = 0
            self.desired_thrust[5] = 0
            self.desired_depth = 0
            if self.currStateTime > 4:
                self.state = "final"
                self.reset_state_time()
        else:
            self.desired_thrust = self.desired_thrust * 0

        if self.currTime > 500:
            self.desired_thrust = self.desired_thrust * 0

        thrust_msg = self.get_thrust_msg(self.desired_thrust)
        self.thrust_publisher_.publish(thrust_msg)

        self.pub_desired_yaw()
        self.pub_desired_depth()

        print(self.state)

    def publish(self, msg):
        self.publisher_.publish(msg)

    def get_thrust_msg(self, thrust_arr):
        currentTeleopMsg = Wrench()
        thrust_arr = np.clip(thrust_arr, -self.thrust_clip, self.thrust_clip)
        currentTeleopMsg.force.x = float(thrust_arr[0])
        currentTeleopMsg.force.y = float(thrust_arr[1])
        currentTeleopMsg.force.z = float(thrust_arr[2])
        currentTeleopMsg.torque.x = float(thrust_arr[3])
        currentTeleopMsg.torque.y = float(thrust_arr[4])
        currentTeleopMsg.torque.z = float(thrust_arr[5])
        return currentTeleopMsg
    
    def yaw_control_callback(self, msg):
        self.controlledYaw = msg.data

    def depth_control_callback(self, msg):
        self.controlledZ = msg.data

    def curr_yaw_callback(self, msg):
        self.currYaw = msg.z
    
    def rel_angle_diff(self, ang1, ang2):
        return (((180 + ang1 - ang2) % 360) - 180)
    
    def gate_callback(self, msg):

        gateX = msg.x
        self.curr_gate_width = msg.z
        angle = (gateX - 1280/2)* (90/1280)

        print("desired gate yaw", self.desired_gate_yaw, " self.curryaw", self.currYaw, ' diff', angle)
        self.desired_gate_yaw = self.desired_gate_yaw + 0.4 * self.rel_angle_diff((self.currYaw - angle), self.desired_gate_yaw)

    def buoy_callback(self, msg):

        buoyX = msg.x - self.wallNegate * (msg.z / 4)
        self.curr_buoy_width = msg.z
        angle = (buoyX - 1280/2)* (90/1280)

        self.desired_buoy_yaw = self.desired_buoy_yaw + 0.4 * self.rel_angle_diff((self.currYaw - angle), self.desired_buoy_yaw)

    def opened_callback(self, msg):

        openedX = msg.x
        if not self.state == "fire":
            depthDelta = ((360) - msg.y) / 5000.0
            new_depth = self.currDepth + depthDelta
            self.desired_torpedo_depth = (0.7 * self.desired_depth) + (0.3 * new_depth)
        self.curr_opened_width = msg.z
        angle = (openedX - 1280/2)* (90/1280)

        self.desired_opened_yaw = self.desired_opened_yaw + 0.4 * self.rel_angle_diff((self.currYaw - angle), self.desired_opened_yaw)

    def iris_callback(self, msg):

        irisX = msg.x
        if self.state == "fire":
            depthDelta = ((360 + 0.8 * msg.z) - msg.y) / 5000.0
            new_depth = self.currDepth + depthDelta
            self.desired_torpedo_depth = (0.7 * self.desired_depth) + (0.3 * new_depth)

        angle = (irisX - 1280/2)* (90/1280)
        if self.left_fired:
            angle = ((irisX - 0.8 * msg.z) - 1280/2)* (90/1280)

        self.curr_iris_width = msg.z

        self.desired_opened_yaw = self.desired_opened_yaw + 0.4 * self.rel_angle_diff((self.currYaw - angle), self.desired_opened_yaw)

    def wall_callback(self, msg):
        angle_diff = msg.x
        dist = msg.y
        self.desired_wall_yaw = self.desired_wall_yaw + 0.4 * self.rel_angle_diff((self.currYaw + angle_diff), self.desired_wall_yaw)
        self.wall_dist = (self.wall_dist * 0.5) + (dist * 0.5)

    def oct_callback(self, msg):
        if msg.z == 1:
            self.octX = msg.x
            self.octY = msg.y
            self.trackOct = True
        else:
            self.trackOct = False


    def fire_left(self):
        msg = Int32()
        msg.data = 0
        self.torpedo_publisher_.publish(msg)

    def fire_right(self):
        msg = Int32()
        msg.data = 2
        self.torpedo_publisher_.publish(msg)

    def center_torpedo(self):
        msg = Int32()
        msg.data = 1
        self.torpedo_publisher_.publish(msg)

    def curr_depth_callback(self, msg):
        self.currDepth = msg.data

    def set_led(self, val):
        msg = Int32()
        msg.data = val
        self.led_publisher_.publish(msg)


if __name__ == '__main__':
    rclpy.init()
    autoNode = AutoNode("AutoNode")
    rclpy.spin(autoNode)

