#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Wrench, Vector3 
from std_msgs.msg import Float32
import math
import numpy as np
import time


class PrequalNode(Node):

    def __init__(self, name, thrust_channel='desired_thrust', z_channel = 'z_control', yaw_channel = 'yaw_control'):
        super().__init__(name)
        self.thrust_publisher_ = self.create_publisher(Wrench, thrust_channel, 1)
        self.des_yaw_publisher_ = self.create_publisher(Float32, 'desired_yaw', 1)
        self.des_depth_publisher_ = self.create_publisher(Float32, 'desired_depth', 1)
        self.subscription_yaw_control = self.create_subscription(
            Float32,
            yaw_channel,
            self.yaw_control_callback,
            5)

        self.subscription_depth_control = self.create_subscription(
            Float32,
            z_channel,
            self.depth_control_callback,
            5)
        
        self.yaw_subscription = self.create_subscription(
            Vector3,
            'rpy',
            self.curr_yaw_callback,
            5)

        self.gate_subscription = self.create_subscription(
            Vector3,
            '/gate_pos',
            self.gate_callback,
            5)

        self.pipe_subscription = self.create_subscription(
            Vector3,
            '/pipe_pos',
            self.pipe_callback,
            5)
        


        timer_period = 0.05  # seconds
        self.timer = self.create_timer(timer_period, self.main_callback)
        self.thrust_clip = np.array([1.4, 1.4, 1.6, 0.8, 0.8, 1.0])
        self.desired_thrust = np.zeros((6))

        self.state = "init" #states are init, gate_center, through_gate, pipe_center, rotate_pipe
        self.controlledZ = 0
        self.controlledYaw = 0
        self.start_yaw = -80.0
        self.desired_yaw = self.start_yaw 
        self.desired_depth = 0
        self.desired_gate_yaw = self.desired_yaw
        self.firstCB = True
        self.currYaw = 0
        self.curr_gate_width = 0
        self.curr_pipe_depth = 1000
        self.desired_pipe_yaw = self.desired_yaw

    def pub_desired_yaw(self):
        des_yaw_msg = Float32()
        des_yaw_msg.data = (float)(self.desired_yaw)
        self.des_yaw_publisher_.publish(des_yaw_msg)

    def pub_desired_depth(self):
        des_depth_msg = Float32()
        des_depth_msg.data = (float)(self.desired_depth)
        self.des_depth_publisher_.publish(des_depth_msg)
    
    def reset_state_time(self):
        self.startStateTime = time.time()
        self.currStateTime = (time.time() - self.startStateTime)

    def main_callback(self):
        self.desired_thrust = np.zeros((6))

        if self.firstCB:
            self.firstCB = False
            self.startTime = time.time()
            self.startStateTime = time.time()

        self.currTime = (time.time() - self.startTime)
        self.currStateTime = (time.time() - self.startStateTime)

        self.desired_thrust[2] = self.controlledZ
        self.desired_thrust[5] = self.controlledYaw

        if self.state == "init":
            if self.currStateTime > 52:
                self.state = "gate_center"
                self.reset_state_time()
            elif self.currStateTime > 30:
                self.desired_depth =  -1.1
            else:
                self.desired_thrust = self.desired_thrust * 0
        elif self.state == "gate_center":
            self.desired_yaw = self.desired_gate_yaw
            
            self.desired_thrust[1] = (1000 - self.curr_gate_width ) / 200 ##
            if self.currStateTime > 16:
                self.state = "gate_through"
                self.reset_state_time()
        elif self.state == "gate_through":
            self.desired_thrust[1] = 2
            if self.currStateTime > 12:
                self.state = "pipe_center"
                self.desired_pipe_yaw = self.start_yaw
                self.reset_state_time()
        elif self.state == "pipe_center":
            self.desired_yaw = self.desired_pipe_yaw
            self.desired_thrust[1] = (self.curr_pipe_depth - 1000 ) / 200
            if self.currStateTime > 20:
                self.state = "pipe_circle"
                self.pipe_end_yaw = self.desired_pipe_yaw
                self.reset_state_time()
        elif self.state == "pipe_circle":
            self.desired_yaw = self.desired_pipe_yaw
            self.desired_thrust[1] = (self.curr_pipe_depth - 1000 ) / 200
            if self.currStateTime > 30:
                self.desired_thrust[0] = 0.7
                error = ((180 + self.pipe_end_yaw - self.currYaw) % 360) - 180
                if abs(error) < 5:
                    self.state = "gate_center_2"
                    self.desired_yaw = self.start_yaw + 180
                    self.desired_gate_yaw = self.desired_yaw
                    self.reset_state_time()
            else:
                self.desired_thrust[0] = 0.7

        elif self.state == "gate_center_2":
            if self.currStateTime < 4:
                self.desired_gate_yaw = self.start_yaw + 180
            else:
                self.desired_yaw = self.desired_gate_yaw
                self.desired_thrust[1] = (1000 - self.curr_gate_width ) / 200 ##

            if self.currStateTime > 16:
                self.state = "gate_through_2"
                self.reset_state_time()
        elif self.state == "gate_through_2":
            self.desired_thrust[1] = 2
            if self.currStateTime > 18:
                self.state = "surface"
                self.reset_state_time()
        elif self.state == "surface":
            self.desired_thrust[1] = 0
            self.desired_thrust[0] = 0
            self.desired_depth = 0
            if self.currStateTime > 8:
                self.state = "final"
                self.reset_state_time()
        else:
            self.desired_thrust = self.desired_thrust * 0

        if self.currTime > 300:
            self.desired_thrust = self.desired_thrust * 0

        thrust_msg = self.get_thrust_msg(self.desired_thrust)
        self.thrust_publisher_.publish(thrust_msg)

        self.pub_desired_yaw()
        self.pub_desired_depth()

        print(self.state)

    def publish(self, msg):
        self.publisher_.publish(msg)

    def get_thrust_msg(self, thrust_arr):
        currentTeleopMsg = Wrench()
        thrust_arr = np.clip(thrust_arr, -self.thrust_clip, self.thrust_clip)
        currentTeleopMsg.force.x = float(thrust_arr[0])
        currentTeleopMsg.force.y = float(thrust_arr[1])
        currentTeleopMsg.force.z = float(thrust_arr[2])
        currentTeleopMsg.torque.x = float(thrust_arr[3])
        currentTeleopMsg.torque.y = float(thrust_arr[4])
        currentTeleopMsg.torque.z = float(thrust_arr[5])
        return currentTeleopMsg
    
    def yaw_control_callback(self, msg):
        self.controlledYaw = msg.data

    def depth_control_callback(self, msg):
        self.controlledZ = msg.data

    def curr_yaw_callback(self, msg):
        self.currYaw = msg.z
    
    def rel_angle_diff(self, ang1, ang2):
        return (((180 + ang1 - ang2) % 360) - 180)
    
    def gate_callback(self, msg):
        if msg.x is None or msg.x == -1:
            return

        print("desired gate yaw", self.desired_gate_yaw, " self.curryaw", self.currYaw, ' diff', msg.z)
        self.desired_gate_yaw = self.desired_gate_yaw + 0.4 * self.rel_angle_diff((self.currYaw - msg.z), self.desired_gate_yaw)
        self.curr_gate_width = msg.y

    def pipe_callback(self, msg):
        if msg.x is None or msg.x < 0:
            return

        self.desired_pipe_yaw = self.desired_pipe_yaw + 0.4 * self.rel_angle_diff(self.currYaw - msg.y, self.desired_pipe_yaw)
        self.curr_pipe_depth = msg.z

if __name__ == '__main__':
    rclpy.init()
    prequalNode = PrequalNode("PrequalNode")
    rclpy.spin(prequalNode)

