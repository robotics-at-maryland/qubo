/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
*/

/* current plan:
   - Set up a topic that listens, upon receiving a problem from the ADS side
     of things send a message here that indicates turning off the switch
   - The ADS side should publish a message upon receiving a problem
*/

#include <iostream>

#include <cstdio>

#include "qubo/i2cdriver.h"
#include "qubo/PCF8574.h"

const int PCF8574::paddr = 0x00;
const int PCF8574::bus_num = 8;

PCF8574::PCF8574() : I2CDevice::I2CDevice(PCF8574::paddr) {
    /* power on should write the defaults */
    int data;

    if (open_i2c(PCF8574::bus_num)) {
        std::cerr << "error opening PCF8574 i2c\n";
        return;
    }

    //figure out if this is the correct byte, and which pins I care about
    if (write_byte(0xFF) < 0) {
        std::cerr << "cannot start up PCF8574\n";
        return;
    }
}

PCF8574::~PCF8574() {
    //do nothing, call parent's destructor
}

PCF8574 &PCF8574::get_instance() {
    static PCF8574 instance;

    return instance;
}

int PCF8574::write_io(u8 data) {
    if (write_byte(data) < 0) {
        std::cerr << "cannot write data to PCF8574\n";
        return -1;
    }

    std::cout << "sucessfully wrote data: " << data << std::endl;

    return 0;
}
