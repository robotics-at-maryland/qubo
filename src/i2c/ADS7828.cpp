/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
*/

#include <cstdio>
#include <cstdint>
#include <vector>

#include "qubo/i2cdriver.h"
#include "qubo/ADS7828.h"

const int ADS7828::paddr = 0x48;
const int ADS7828::bus_num = 8;

const int ADS7828::PRECISION_BITS = 12;
const int ADS7828::PRECISION = 1 << PRECISION_BITS;
const float ADS7828::REF_VOLTAGE = 5.13f;

ADS7828::ADS7828() : I2CDevice::I2CDevice(ADS7828::paddr) {
    LOG("<ADS7828>: opening i2c");

    if (open_i2c(ADS7828::bus_num)) {
        LOG("<ADS7828>: error opening i2c");
        return;
    }
}

ADS7828::~ADS7828() {
    
}

ADS7828 &ADS7828::get_instance() {
    static ADS7828 instance;

    return instance;
}

/* look into the i2c_smbus_read_word_data documentation for this,
   it matches up directly with the ADS documentation */

/* input a channel number as it is on the chip, 0 through 7 */
int ADS7828::read_channel(int channelno) {
    u8 cmd;

    std::cerr << "<ADS7828>: attempting to read from channel " << channelno << "\n";

    if (channelno < 0 || 7 < channelno) {
        std::cerr << "INVALID CHANNEL ENTRY";
        return -1;
    }

    u8 conv[8] = {0, 4, 1, 5, 2, 6, 3, 7};
    std::printf("a bigger test: %d\n", conv[3]);

    std::printf("channel gets converted to: %d\n", conv[channelno]);
    // use single-ended inputs, external reference, and turn on converter
    cmd = 0x80 | (conv[channelno] << 4) | 0x04;
    std::printf("the command being sent: %2x\n", cmd);

    return read_word(cmd);
}

float ADS7828::read_channel_voltage(int channelno) {
    int word = read_channel(channelno);
    int lsb = word & 0xFF;
    word = (word >> 8) | (lsb << 8);
    if (word == -1) {
        return 0.0f;
    }

    std::printf("voltage word after swapping: %4x\n", word);
    //voltage reading comes in scaled to REF_VOLTAGE / 4096
    float voltage = word * REF_VOLTAGE / PRECISION;
    std::cout << "voltage after scaling: " << voltage << std::endl;

    // reading is 10% of what it actually is
    voltage *= 10.0f;
    std::cout << "voltage after multiplying by 10: " << voltage << std::endl;

    return voltage;
}

float ADS7828::read_channel_current(int channelno) {
    int word = read_channel(channelno);
    int lsb = word & 0xFF;
    word = (word >> 8) | (lsb << 8);
    if (word == -1) {
        return 0.0f;
    }

    //current reading comes in scaled to REF_VOLTAGE / PRECISION
    float current = word * REF_VOLTAGE / PRECISION;

    //offset of 0.5 V
    current -= 0.513f;
    
    // scale is 200 mV/A
    current *= 5.0f;

    std::cout << "current after scaling: " << current << std::endl;
    return current;
}



