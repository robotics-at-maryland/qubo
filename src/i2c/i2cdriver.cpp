#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include "qubo/i2cdriver.h"
#include "qubo/PCA9685.h"

extern "C" {
#include <mpsse.h>
#include <unistd.h>
}

const int I2CDevice::FT232H_VID = 0x0403;
const int I2CDevice::FT232H_PID = 0x6014;
// const int I2CDevice::I2C_FREQ = ONE_HUNDRED_KHZ;
const int I2CDevice::I2C_FREQ = ONE_MHZ;
mpsse_context *I2CDevice::context = NULL;
I2CDevice::I2CDevice(uint16_t paddr) : addr(paddr) {

}

I2CDevice::~I2CDevice() {
    if (context != NULL)
        close_i2c();
}

// the old driver relied on num to open the bus, for compatibility keep this
// but right now it is unused
int I2CDevice::open_i2c(int num) {
    (void) num;

    if (context == NULL)
      context = Open(FT232H_VID, FT232H_PID, I2C,
		     I2C_FREQ, MSB, IFACE_ANY, NULL, NULL);
    
    if (!context->open) {
        std::fprintf(stderr, "Error: Could not find FTDI device!\n");
        return -1;
    }

    return 0;
}

int I2CDevice::close_i2c() {
    Close(context);
    context = NULL;
    
    return 0;
}

int I2CDevice::write_byte(uint8_t data) {
    Start(context);
    if (Write(context, reinterpret_cast<char *>(&data), 1) == MPSSE_FAIL) {
        std::fprintf(stderr, "Error: Failure writing byte\n");
        return -1;
    }
    Stop(context);

    return 0;
}

int I2CDevice::write_byte_reg(uint8_t reg, uint8_t data) {
    uint8_t temp_addr = this->addr << 1;
    
    Start(context);
    Write(context, reinterpret_cast<char *>(&temp_addr), 1);
    Write(context, reinterpret_cast<char *>(&reg), 1);
    Write(context, reinterpret_cast<char *>(&data), 1);
    Stop(context);

    return 0;
}

int I2CDevice::read_byte(uint8_t reg) {
    char *buf, data;
    uint8_t temp_addr = this->addr << 1;

    std::printf("%x\n", temp_addr);
    
    Start(context);
    Write(context, reinterpret_cast<char *>(&temp_addr), 1);
    Write(context, reinterpret_cast<char *>(&reg), 1);
    Start(context);

    temp_addr |= 0x01;
    Write(context, reinterpret_cast<char *>(&temp_addr), 1);
    buf = Read(context, 1);
    SendNacks(context);
    Read(context, 1);
    Stop(context);
    
    data = *buf;
    
    SendAcks(context);
    std::free(buf);
    return data;
}

int I2CDevice::read_word(uint8_t cmd) {
    (void) cmd;
    
    std::fprintf(stderr, "WARNING: read_word IS NOT IMPLEMENTED!\n");
    return -1;
}
