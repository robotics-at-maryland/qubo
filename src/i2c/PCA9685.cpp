/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
*/

#include <iostream>

#include <cmath>
#include <cstdio> // for debugging

#include "qubo/i2cdriver.h"
#include "qubo/PCA9685.h"

PCA9685 thruster_controller[2] = {
    PCA9685(PCA9685::THRUSTER_BUS1, PCA9685::THRUSTER_ADDR1),
    PCA9685(PCA9685::THRUSTER_BUS2, PCA9685::THRUSTER_ADDR2),
};
PCA9685 pneumatics_controller(PCA9685::PNEUMATICS_BUS, PCA9685::PNEUMATICS_ADDR);

const int PCA9685::THRUSTER_BUS1 = 1;
const int PCA9685::THRUSTER_ADDR1 = 0x41;

const int PCA9685::THRUSTER_BUS2 = 1;
const int PCA9685::THRUSTER_ADDR2 = 0x47;

const int PCA9685::PNEUMATICS_BUS = 1;
const int PCA9685::PNEUMATICS_ADDR = 0x43;

const int PCA9685::pwm_freq_hz = 200;
const int PCA9685::delay_percent = 10;
const int PCA9685::PCA9685_ledn_prec = 4096;
//const int PCA9685::paddr = 0x41;
//const int PCA9685::bus_num = 8;
const u8 PCA9685::PWM_LED[16] = {0x06, 0x0A, 0x0E, 0x12,
                                 0x16, 0x1A, 0x1E, 0x22,
                                 0x26, 0x2A, 0x2E, 0x32,
                                 0x36, 0x3A, 0x3E, 0x42};

PCA9685::PCA9685(const int bus, const int addr) : I2CDevice::I2CDevice(addr) {
    int data;
    
    // Somehow figure out which i2c bus we are working with
    // on the raspberry pi this is 1
    LOG("<PCA9685>: opening i2c");

    if (open_i2c(bus)) {
        std::cerr << "error opening i2c\n";
        // there was an error, don't do anything,
        // maybe throw exception here depending on what is idiomatic for ROS
        return;
    }

    if ((data = read_byte(0x00)) < 0) {
        // error, throw exception?
        std::cerr << "there was an error\n";
        return;
    }

    // if errors for later write bytes, consider throwing an exception
    
    if (data & PCA9685_SLEEP)
        write_byte_reg(0x00, 0x00); // NOTE: we are turning off ALLCALL here

    // Make updates go into effect after writing all 4 LED registers
    // Also keep totem-pole structure (not sure what this means,
    // but it's the default so I'm keeping it this way)
    write_byte_reg(0x01, 0x0C); 
}

PCA9685::~PCA9685() {
    // automatically calls parent's destructor
}

PCA9685 &PCA9685::get_instance() {
    static PCA9685 instance(1, 1);

    return instance;
}

int PCA9685::write_pwm(const int pwm, const int channel) {
    u8 bytes[4];
    pwm_to_ledn(bytes, pwm);

    for (int i = 0; i < 4; i++) {
        std::printf("writing byte 0x%02x to channel 0x%02x\n",
                    bytes[i],
                    channel + i);
                    
        write_byte_reg(channel + i, bytes[i]);
    }
    
    return 1;
}

// **************WARNING***************
// Do not use write_on or write_off with thruster channels!!!!!!

int PCA9685::write_on(const int channel) {
    u8 bytes[4];

    bytes[0] = 0x00;
    bytes[1] = 0x00;
    bytes[2] = 0xFF;
    bytes[3] = 0x0F;

    for (int i = 0; i < 4; i++) {
        std::printf("writing byte 0x%02x to channel 0x%02x\n",
                    bytes[i],
                    channel + i);
                    
        write_byte_reg(channel + i, bytes[i]);
    }

    return 0;
}

int PCA9685::write_off(const int channel) {
    u8 bytes[4];

    bytes[0] = 0x00;
    bytes[1] = 0x00;
    bytes[2] = 0x00;
    bytes[3] = 0x00;

    for (int i = 0; i < 4; i++) {
        std::printf("writing byte 0x%02x to channel 0x%02x\n",
                    bytes[i],
                    channel + i);
                    
        write_byte_reg(channel + i, bytes[i]);
    }

    return 0;
}

int PCA9685::write_full(const bool on, const int channel) {
    u8 bytes[4];

    if (!(channel == PCA9685::PWM_LED[10] || channel == PCA9685::PWM_LED[11])){
        std::printf("only allow full on/off write to channels 10 and 11, not the one you provided");
        return 1;
    }

    if(on){
	bytes[0] = 0xFE;
	bytes[1] = 0x1F;
	bytes[2] = 0x00;
	bytes[3] = 0x00;
    }else{
	bytes[0] = 0x99;
	bytes[1] = 0x01;
	bytes[2] = 0x99;
	bytes[3] = 0x01;
    }

    for (int i = 0; i < 4; i++) {
        std::printf("writing byte 0x%02x to channel 0x%02x\n",
                    bytes[i],
                    channel + i);
                    
        write_byte_reg(channel + i, bytes[i]);
    }
    
    return 1;
}

bool PCA9685::pwm_to_ledn(u8 *bytes, const int pwm) const {
    double period_us = 1000.0 * 1000 / pwm_freq_hz;
    double duty = (pwm / period_us) * 100;
    bool success = duty <= 100.0;
    std::printf("converting pwm %d to duty %lf%%\n", pwm, duty);

    if (!success) {
        // eventually implement functionality for this 
        LOG("<PCA9685>: WARNING: attempting to write duty above 100%");
    }

    return success ? duty_to_ledn(bytes, duty) : false;
}

bool PCA9685::duty_to_ledn(u8 *bytes, const double percent) const {
    unsigned int delay = duty_to_count(delay_percent);
    std::printf("converting duty %lf%% to count %u\n", percent, duty_to_count(percent));
    
    // First write the delay
    bytes[0] = (delay - 1) & 0xFF;
    bytes[1] = ((delay - 1) >> 8) & 0xFF;

    // Now write the given PWM
    unsigned int stop = delay + duty_to_count(percent);
    bytes[2] = (stop - 1) & 0xFF;
    bytes[3] = ((stop - 1) >> 8) & 0xFF;
    
    // insert a check for setting the MSBs
    if ((bytes[1] & 0xE0) || (bytes[3] & 0xE0)) {
        std::cerr << "Warning: writing the most significant bits in "
                  << "registers, this is undefined behavior\n";
        return false;
    }
    
    return true;
}

unsigned int PCA9685::duty_to_count(const double percent) const {
    return std::lround((PCA9685_ledn_prec * percent) / 100.0);
}
