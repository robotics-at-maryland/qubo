/* 2024 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
 */

// ROS includes
#include "rclcpp/rclcpp.hpp"

// this is bad, make a custom boolean message type here
#include "std_msgs/msg/u_int8_multi_array.hpp"
#include "std_msgs/msg/bool.hpp"

#include "qubo/PCA9685.h"
#include "qubo/pneumatics.h"

int PneumaticsSubscriber::solenoid_map[4] = {
    PCA9685::PWM_LED[0],
    PCA9685::PWM_LED[2],
    PCA9685::PWM_LED[1],
    PCA9685::PWM_LED[3]
};

const int PneumaticsSubscriber::MARKER_SOLENOID = PCA9685::PWM_LED[0];
const int PneumaticsSubscriber::CLAW_SOLENOID = PCA9685::PWM_LED[2];
const int PneumaticsSubscriber::TORPEDO_SOLENOID = PCA9685::PWM_LED[1];

PneumaticsSubscriber::PneumaticsSubscriber()
    : Node("pneumatics_subscriber"), pwm_hat(pneumatics_controller) {
    
    subscription_ = this->create_subscription<PneumaticsMsg>
        ("desired_solenoid",
         10,
         std::bind(&PneumaticsSubscriber::topic_callback, this, _1));
    marker_subscription_ = this->create_subscription<std_msgs::msg::Bool>(
        "set_marker",
        10,
        std::bind(&PneumaticsSubscriber::marker_callback, this, _1)
    );
    claw_subscription_ = this->create_subscription<std_msgs::msg::Bool>(
        "set_claw",
        10,
        std::bind(&PneumaticsSubscriber::claw_callback, this, _1)
    );
    torpedo_subscription_ = this->create_subscription<std_msgs::msg::Bool>(
        "set_torpedo",
        10,
        std::bind(&PneumaticsSubscriber::torpedo_callback, this, _1)
    );

    
    RCLCPP_INFO(this->get_logger(),
                "<pneumatics>: Subscriber start");
}

void PneumaticsSubscriber::topic_callback(const PneumaticsMsg::SharedPtr msg) {
    for (int i = 0; i < 4; i++) {
        if (msg->data[i]) {
            RCLCPP_INFO(this->get_logger(),
                        "<pneumatics>: turning solenoid %d on", i);
            pwm_hat.write_on(solenoid_map[i]);
        } else {
            RCLCPP_INFO(this->get_logger(),
                        "<pneumatics>: turning solenoid %d off", i);
            pwm_hat.write_off(solenoid_map[i]);
        }
    }
}

void PneumaticsSubscriber::marker_callback(const std_msgs::msg::Bool::SharedPtr msg) {
    if (msg->data) {
        RCLCPP_INFO(this->get_logger(),
                    "<pneumatics>: turning marker solenoid on");
        pwm_hat.write_on(MARKER_SOLENOID);
    } else {
        RCLCPP_INFO(this->get_logger(),
                    "<pneumatics>: turning marker solenoid off");
        pwm_hat.write_off(MARKER_SOLENOID);
    }
}

void PneumaticsSubscriber::claw_callback(const std_msgs::msg::Bool::SharedPtr msg) {
    if (msg->data) {
        RCLCPP_INFO(this->get_logger(),
                    "<pneumatics>: turning claw solenoid on");
        pwm_hat.write_on(CLAW_SOLENOID);
    } else {
        RCLCPP_INFO(this->get_logger(),
                    "<pneumatics>: turning claw solenoid off");
        pwm_hat.write_off(CLAW_SOLENOID);
    }
}

void PneumaticsSubscriber::torpedo_callback(const std_msgs::msg::Bool::SharedPtr msg) {
    if (msg->data) {
        RCLCPP_INFO(this->get_logger(),
                    "<pneumatics>: turning torpedo solenoid on");
        pwm_hat.write_on(TORPEDO_SOLENOID);
    } else {
        RCLCPP_INFO(this->get_logger(),
                    "<pneumatics>: turning torpedo solenoid off");
        pwm_hat.write_off(TORPEDO_SOLENOID);
    }
}

int main(int argc, char *argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<PneumaticsSubscriber>());
    rclcpp::shutdown();

    return 0;
}


