/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
   Josh Smith
 */

#include "qubo/thrusters.h"

const double lateral_f = 1.0 / (2.0*sqrt(2));
const Eigen::Matrix<double, 8, 6> ThrusterSubscriber::jetson_matrix {
    { 0,            0,          0.5,   -0.5,   -0.5,    0},
    { 0,            0,          0.5,    0.5,    0.5,    0},
    { 0,            0,          0,     -0.5,    0.5,    0},
    { 0,            0,          0,      0.5,   -0.5,    0},
    { lateral_f,    lateral_f,  0,      0,      0,      0.615182},
    { lateral_f,    lateral_f,  0,      0,      0,     -0.615182},
    { lateral_f,   -lateral_f,  0,      0,      0,      0},
    { lateral_f,   -lateral_f,  0,      0,      0,      0}
};

// CORRESPONDING WITH CURRENT THRUSTER LABELS ON QUBO:
// FRONT    BOTTOM  RIGHT   (0 ON QUBO) (0 ON CARD 1) - POSITIVE THRUST -> +Z
// BACK     BOTTOM  LEFT    (1 ON QUBO) (1 ON CARD 1) - POSITIVE THRUST -> +Z
// BACK     BOTTOM  RIGHT   (2 ON QUBO) (2 ON CARD 1) - POSITIVE THRUST -> +Z
// FRONT    BOTTOM  LEFT    (3 ON QUBO) (3 ON CARD 1) - POSITIVE THRUST -> +Z
// FRONT    TOP     RIGHT   (4 ON QUBO) (0 ON CARD 2) - POSITIVE THRUST -> +X
// BACK     TOP     LEFT    (5 ON QUBO) (1 ON CARD 2) - POSITIVE THRUST -> +X
// BACK     TOP     RIGHT   (6 ON QUBO) (2 ON CARD 2) - POSITIVE THRUST -> +X
// FRONT    TOP     LEFT    (7 ON QUBO) (3 ON CARD 2) - POSITIVE THRUST -> +X

int ThrusterSubscriber::thruster_map_card1[4] = { // ZERO THRUST 1630
    PCA9685::PWM_LED[0], // FRONT   BOTTOM  RIGHT
    PCA9685::PWM_LED[1], // BACK    BOTTOM  LEFT    REVERSED
    PCA9685::PWM_LED[2], // BACK    BOTTOM  RIGHT
    PCA9685::PWM_LED[3]  // FRONT   BOTTOM  LEFT    REVERSED
};
int ThrusterSubscriber::thruster_map_card2[4] = { // ZERO THRUST 1575
    PCA9685::PWM_LED[0], // FRONT   TOP     RIGHT 
    PCA9685::PWM_LED[1], // BACK    TOP     LEFT
    PCA9685::PWM_LED[2], // BACK    TOP     RIGHT   REVERSED
    PCA9685::PWM_LED[3]  // FRONT   TOP     LEFT    REVERSED
};

const PWMVector ThrusterSubscriber::zero_thrust =
  {1500, 1500, 1500, 1500, 1500, 1500, 1500, 1500};

ThrusterSubscriber::ThrusterSubscriber()
    : Node("thruster_subscriber"), pwm_hat1(thruster_controller[0]),
      pwm_hat2(thruster_controller[1]) {

    add_desired_thrust_sub = this->create_subscription<WrenchStamped>(
        "/desired_thrust/add",
        10,
        std::bind(&ThrusterSubscriber::add_desired_thrust_callback, this, _1)
    );
    remove_desired_thrust_sub = this->create_subscription<String>(
        "/desired_thrust/remove",
        10,
        std::bind(&ThrusterSubscriber::remove_desired_thrust_callback, this, _1)
    );
    combine_desired_thrusts_timer = this->create_wall_timer(
        std::chrono::milliseconds(20),
        std::bind(&ThrusterSubscriber::combine_desired_thrusts, this)
    );
    reset_desired_thrusts_sub = this->create_subscription<Empty>(
        "/desired_thrust/reset",
        10,
        std::bind(&ThrusterSubscriber::reset_desired_thrusts_callback, this, _1)
    );
    desired_thrusts = {};

    arm_thrust();
}

void ThrusterSubscriber::add_desired_thrust_callback(const WrenchStamped::SharedPtr msg) {
    ForceVector v;
    Wrench wrench = msg->wrench;
    v << wrench.force.x, wrench.force.y, wrench.force.z,
         wrench.torque.x, wrench.torque.y, wrench.torque.z;
    desired_thrusts[msg->header.frame_id] = std::pair<rclcpp::Time, ForceVector>(
        Node::get_clock()->now(),
        v
    );
}

void ThrusterSubscriber::remove_desired_thrust_callback(const String::SharedPtr msg) {
    if (!desired_thrusts.erase(msg->data)) {
        RCLCPP_INFO(this->get_logger(),
            "remove_desired_thrust: invalid thrust %s", msg->data.c_str()
        );
    }
}

void ThrusterSubscriber::combine_desired_thrusts() {
    ForceVector final_thrust_vector = {0, 0, 0, 0, 0, 0};
    std::vector<std::string> expired_thrusts;
    for (auto& pair : desired_thrusts) {
        if (pair.second.first < Node::get_clock()->now() - rclcpp::Duration(1, 0)) {
            RCLCPP_INFO(this->get_logger(),
                "combine_desired_thrusts: removing thrust %s last published at %f", pair.first.c_str(), pair.second.first.seconds()
            );
            expired_thrusts.push_back(pair.first);
        } else {
            RCLCPP_INFO(this->get_logger(),
                "combine_desired_thrusts: adding thrust %s last published at %f", pair.first.c_str(), pair.second.first.seconds()
            );
            final_thrust_vector += pair.second.second;
        }
    }

    for (const auto& thrust : expired_thrusts) {
        desired_thrusts.erase(thrust);
    }

    std::stringstream debug_final_thrust_vector;
    for (int i = 0; i < final_thrust_vector.size(); i++) {
        debug_final_thrust_vector << final_thrust_vector(i) << " ";
    }
    RCLCPP_INFO(this->get_logger(),
        "final_thrust_vector: %s", debug_final_thrust_vector.str().c_str()
    );

    PWMVector pwm = jetson_matrix * final_thrust_vector;
    force_to_pwm(12, pwm);
    flip_pwm_vec(pwm);

    write_pwm(pwm);
}

void ThrusterSubscriber::reset_desired_thrusts_callback(Empty::SharedPtr msg) {
    (void) msg;
    desired_thrusts.clear();
}

/* This function inverts the pwm so that positive force yields the direction of efficient thruster spin/force, to account for non-symmetric pwm/trhrust relationship*/
void ThrusterSubscriber::flip_pwm_vec(PWMVector &pwm) {
    int i = 0;
    for (auto &p : pwm) {
        if (i == 1 || i == 3 || i == 6 || i == 7){
            p = 3000 - p;
        }
        i += 1;
    }
}

void ThrusterSubscriber::arm_thrust() {
     write_pwm(ThrusterSubscriber::zero_thrust);
}

void ThrusterSubscriber::write_pwm(const PWMVector &pwm) {
    int i = 0;

    // for the first card
    for (auto id : thruster_map_card1) {
        pwm_hat1.write_pwm(pwm[i++] + 130, id);
    }

    // for the second card
    for (auto id : thruster_map_card2) {
        pwm_hat2.write_pwm(pwm[i++] + 75, id);
    }
}

void ThrusterSubscriber::force_to_pwm(int voltage, PWMVector &pwm) {
    for (auto &force : pwm.reshaped()) {
        if (force > 0) {
            if (voltage < 12)
                force = (-1.3 + sqrt(7.44 * force + 0.8258)) / 3.72;
            else if (voltage < 14)
                force = (-1.62 + sqrt(9.48 * force + 3.8094)) / 4.74;
            else if (voltage < 16)
                force = (-1.95 + sqrt(11.6 * force + 5.4149)) / 5.8;
            else if (voltage < 18)
                force = (-2.3 + sqrt(13.2 * force + 7.3492)) / 6.6;
            else if (voltage < 20)
                force = (-1.68 + sqrt(18.4 * force + 4.41952)) / 9.2;
            else /*voltage >= 20*/
                force = (-0.822 + sqrt(24.1 * force + 0.720454)) / 12.1;
        } else if (force < 0) {
            if (voltage < 12)
                force = (-1.08 + sqrt(1.708404 - 5.64 * force)) / -2.82;
            else if (voltage < 14)
                force = (-1.41 + sqrt(2.78838 - 6.84 * force)) / -3.42;
            else if (voltage < 16)
                force = (-1.72 + sqrt(4.05964 - 8.28 * force)) / -4.14;
            else if (voltage < 18)
                force = (-1.95 + sqrt(5.15914 - 9.76 * force)) / -4.88;
            else if (voltage < 20)
                force = (-1.86 + sqrt(4.88504 - 12.08 * force)) / -6.04;
            else
                force = (-1.31 + sqrt(2.614564 - 15.68 * force)) / -7.84;
        } else {
            force = 0;
        }

        force = 1500.0 + 400.0 * force;
    }
}


int main(int argc, char *argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ThrusterSubscriber>());
    rclcpp::shutdown();

    return 0;
}
