#!/usr/bin/python3
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from PID import PID
from std_msgs.msg import Float64, Empty, Bool
from geometry_msgs.msg import WrenchStamped
import time
from scipy.spatial.transform import Rotation

class PitchPID(Node):
    def __init__(self):
        super().__init__("pitch_pid_node")
        self.pitch_pid = PID(0.05, 0.0, 0.02)
        self.prev_time = None
        self.desired_pitch = 0
        self.pitch_control_on = True
        self.create_subscription(
            Float64,
            "/desired_position/pitch",
            self.set_desired_pitch,
            5
        )
        self.create_subscription(
            Bool,
            "/desired_position/pitch/control_on",
            self.set_control_on,
            5
        )
        self.create_subscription(
            Empty,
            "/desired_position/pitch/reset",
            self.reset,
            5
        )
        self.create_subscription(
            Odometry,
            "/odometry/filtered",
            self.publish_desired_thrust,
            5
        )
        self.desired_thrust_pub = self.create_publisher(
            WrenchStamped,
            "/desired_thrust/add",
            5
        )

    def set_desired_pitch(self, msg : Float64):
        self.desired_pitch = msg.data
        self.get_logger().info(f"Setting Target Pitch to {msg.data}")

    def set_control_on(self, msg : Bool):
        self.pitch_control_on = msg.data
        self.get_logger().info(f"Setting Pitch Control {"on" if msg.data else "off"}")

    def publish_desired_thrust(self, msg : Odometry):
        quat = msg.pose.pose.orientation
        cur_rot = Rotation.from_quat([quat.x, quat.y, quat.z, quat.w])
        cur_eul = cur_rot.as_euler("xyz", True)
        err = self.desired_pitch - cur_eul[1]
        if err > 180.0:
            err = err - 360.0 if err > 0.0 else err + 360.0
        cur_time = time.time()
        if self.prev_time is None:
            dt = None
        else:
            dt = cur_time - self.prev_time
        self.prev_time = cur_time
        if not self.pitch_control_on:
            out = 0
        else:
            out = self.pitch_pid.run(err, dt)
        wrenchs = WrenchStamped()
        wrenchs.header.frame_id = "pitch_pid"
        wrenchs.header.stamp = self.get_clock().now().to_msg()
        wrenchs.wrench.torque.y = out
        self.get_logger().info(f"CurPitch: {cur_eul[1]}, DesPitch: {self.desired_pitch}, out: {out}")
        self.desired_thrust_pub.publish(wrenchs)

    def reset(self, msg : Empty):
        self.prev_time = None
        self.pitch_pid.reset()
        self.get_logger().info("Resetting Pitch PID")
    
def main():
    rclpy.init()
    pitch_pid = PitchPID()
    rclpy.spin(pitch_pid)

if __name__ == "__main__":
    main()