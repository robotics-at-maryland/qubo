#!/usr/bin/python3
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from PID import PID
from std_msgs.msg import Float64, Empty, Bool
from geometry_msgs.msg import WrenchStamped
import time
from scipy.spatial.transform import Rotation

class YPID(Node):
    def __init__(self):
        super().__init__("y_pid_node")
        self.y_pid = PID(1.0, 0.0, 0.0)
        self.prev_time = None
        self.desired_y = 0
        self.y_control_on = True
        self.create_subscription(
            Float64,
            "/desired_position/y",
            self.set_desired_y,
            5
        )
        self.create_subscription(
            Bool,
            "/desired_position/y/control_on",
            self.set_control_on,
            5
        )
        self.create_subscription(
            Empty,
            "/desired_position/y/reset",
            self.reset,
            5
        )
        self.create_subscription(
            Odometry,
            "/odometry/filtered",
            self.publish_desired_thrust,
            5
        )
        self.desired_thrust_pub = self.create_publisher(
            WrenchStamped,
            "/desired_thrust/add",
            5
        )

    def set_desired_y(self, msg : Float64):
        self.desired_y = msg.data
        self.get_logger().info(f"Setting Target Y to {msg.data}")

    def set_control_on(self, msg : Bool):
        self.y_control_on = msg.data
        self.get_logger().info(f"Setting Y Control {"on" if msg.data else "off"}")

    def publish_desired_thrust(self, msg : Odometry):
        quat = msg.pose.pose.orientation
        cur_rot = Rotation.from_quat([quat.x, quat.y, quat.z, quat.w])
        pos = msg.pose.pose.position
        err = self.desired_y - pos.y
        cur_time = time.time()
        if self.prev_time is None:
            dt = None
        else:
            dt = cur_time - self.prev_time
        self.prev_time = cur_time
        if not self.y_control_on:
            out = 0
        else:
            out = self.y_pid.run(err, dt)
            out = cur_rot.apply([out, 0, 0], inverse=True)
        wrenchs = WrenchStamped()
        wrenchs.header.frame_id = "y_pid"
        wrenchs.header.stamp = self.get_clock().now().to_msg()
        wrenchs.wrench.force.x = out[0]
        wrenchs.wrench.force.y = out[1]
        wrenchs.wrench.force.z = out[2]
        self.get_logger().info(f"CurY: {pos.y}, DesY: {self.desired_y}, out: {out}")
        self.desired_thrust_pub.publish(wrenchs)

    def reset(self, msg : Empty):
        self.prev_time = None
        self.y_pid.reset()
        self.get_logger().info("Resetting Y PID")
    
def main():
    rclpy.init()
    y_pid = YPID()
    rclpy.spin(y_pid)

if __name__ == "__main__":
    main()