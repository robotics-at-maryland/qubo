import numpy as np
from collections import deque

class PID:
    def __init__(self, kP, kI, kD, d_q_size = 5.0, i_min = 5.0, i_max = 5.0) -> None:
        self.kP, self.kI, self.kD = kP, kI, kD
        self.prev_err = None
        self.d_q = deque([0.0] * d_q_size)
        self.d_q_size = d_q_size
        self.i_min, self.i_max = i_min, i_max
        self.I = 0.0

    def run(self, err, dt) -> float:
        P = err
        if dt is not None:
            self.I += err * dt
        self.I = np.clip(self.I, self.i_min, self.i_max)
        if self.prev_err is None:
            D = 0.0
        else:
            d = (err - self.prev_err) / dt
            self.d_q.append(d)
            if len(self.d_q) > self.d_q_size:
                self.d_q.popleft()
            D = np.mean(self.d_q)
        self.prev_err = err

        result = (self.kP * P) + (self.kI * self.I) + (self.kD * D)
        return result
    
    def reset(self):
        self.I = 0.0
        self.prev_err = None
        self.d_q.clear()

    def set_gain(self, kP = None, kI = None, kD = None):
        if kP is not None: self.kP = kP
        if kI is not None: self.kI = kI
        if kD is not None: self.kD = kD