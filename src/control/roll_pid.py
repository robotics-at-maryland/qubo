#!/usr/bin/python3
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from PID import PID
from std_msgs.msg import Float64, Empty, Bool
from geometry_msgs.msg import WrenchStamped
import time
from scipy.spatial.transform import Rotation

class RollPID(Node):
    def __init__(self):
        super().__init__("roll_pid_node")
        self.roll_pid = PID(0.05, 0.0, 0.02)
        self.prev_time = None
        self.desired_roll = 0
        self.roll_control_on = True
        self.create_subscription(
            Float64,
            "/desired_position/roll",
            self.set_desired_roll,
            5
        )
        self.create_subscription(
            Bool,
            "/desired_position/roll/control_on",
            self.set_control_on,
            5
        )
        self.create_subscription(
            Empty,
            "/desired_position/roll/reset",
            self.reset,
            5
        )
        self.create_subscription(
            Odometry,
            "/odometry/filtered",
            self.publish_desired_thrust,
            5
        )
        self.desired_thrust_pub = self.create_publisher(
            WrenchStamped,
            "/desired_thrust/add",
            5
        )

    def set_desired_roll(self, msg : Float64):
        self.desired_roll = msg.data
        self.get_logger().info(f"Setting Target Roll to {msg.data}")

    def set_control_on(self, msg : Bool):
        self.roll_control_on = msg.data
        self.get_logger().info(f"Setting Roll Control {"on" if msg.data else "off"}")

    def publish_desired_thrust(self, msg : Odometry):
        quat = msg.pose.pose.orientation
        cur_rot = Rotation.from_quat([quat.x, quat.y, quat.z, quat.w])
        cur_eul = cur_rot.as_euler("xyz", True)
        err = self.desired_roll - cur_eul[0]
        if err > 180.0:
            err = err - 360.0 if err > 0.0 else err + 360.0
        cur_time = time.time()
        if self.prev_time is None:
            dt = None
        else:
            dt = cur_time - self.prev_time
        self.prev_time = cur_time
        if not self.roll_control_on:
            out = 0
        else:
            out = self.roll_pid.run(err, dt)
        wrenchs = WrenchStamped()
        wrenchs.header.frame_id = "roll_pid"
        wrenchs.header.stamp = self.get_clock().now().to_msg()
        wrenchs.wrench.torque.x = out
        self.get_logger().info(f"CurRoll: {cur_eul[0]}, DesRoll: {self.desired_roll}, out: {out}")
        self.desired_thrust_pub.publish(wrenchs)

    def reset(self, msg : Empty):
        self.prev_time = None
        self.roll_pid.reset()
        self.get_logger().info("Resetting Roll PID")
    
def main():
    rclpy.init()
    roll_pid = RollPID()
    rclpy.spin(roll_pid)

if __name__ == "__main__":
    main()