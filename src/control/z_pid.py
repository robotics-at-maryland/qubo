#!/usr/bin/python3
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from PID import PID
from std_msgs.msg import Float64, Empty, Bool
from geometry_msgs.msg import WrenchStamped
import time
from scipy.spatial.transform import Rotation

class ZPID(Node):
    def __init__(self):
        super().__init__("Z_pid_node")
        self.Z_pid = PID(1.0, 0.0, 0.0)
        self.prev_time = None
        self.desired_z = 0
        self.z_control_on = True
        self.create_subscription(
            Float64,
            "/desired_position/z",
            self.set_desired_z,
            5
        )
        self.create_subscription(
            Bool,
            "/desired_position/z/control_on",
            self.set_control_on,
            5
        )
        self.create_subscription(
            Empty,
            "/desired_position/z/reset",
            self.reset,
            5
        )
        self.create_subscription(
            Odometry,
            "/odometry/filtered",
            self.publish_desired_thrust,
            5
        )
        self.desired_thrust_pub = self.create_publisher(
            WrenchStamped,
            "/desired_thrust/add",
            5
        )

    def set_desired_z(self, msg : Float64):
        self.desired_z = msg.data
        self.get_logger().info(f"Setting Target Z to {msg.data}")

    def set_control_on(self, msg : Bool):
        self.z_control_on = msg.data
        self.get_logger().info(f"Setting Z Control {"on" if msg.data else "off"}")

    def publish_desired_thrust(self, msg : Odometry):
        quat = msg.pose.pose.orientation
        cur_rot = Rotation.from_quat([quat.x, quat.y, quat.z, quat.w])
        pos = msg.pose.pose.position
        err = self.desired_z - pos.z
        cur_time = time.time()
        if self.prev_time is None:
            dt = None
        else:
            dt = cur_time - self.prev_time
        self.prev_time = cur_time
        if not self.z_control_on:
            out = 0
        else:
            out = self.z_pid.run(err, dt)
            out = cur_rot.apply([0, 0, out], inverse=True)
        wrenchs = WrenchStamped()
        wrenchs.header.frame_id = "z_pid"
        wrenchs.header.stamp = self.get_clock().now().to_msg()
        wrenchs.wrench.force.x = out[0]
        wrenchs.wrench.force.y = out[1]
        wrenchs.wrench.force.z = out[2]
        self.get_logger().info(f"CurZ: {pos.z}, DesZ: {self.desired_z}, out: {out}")
        self.desired_thrust_pub.publish(wrenchs)

    def reset(self, msg : Empty):
        self.prev_time = None
        self.z_pid.reset()
        self.get_logger().info("Resetting Z PID")
    
def main():
    rclpy.init()
    z_pid = ZPID()
    rclpy.spin(z_pid)

if __name__ == "__main__":
    main()