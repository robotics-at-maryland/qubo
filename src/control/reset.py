#!/usr/bin/env python3

import rclpy
from rclpy.node import Node, Publisher
from geometry_msgs.msg import PoseWithCovarianceStamped
from std_msgs.msg import Empty
from dvl_msgs.msg import ConfigCommand

class Reset(Node):
    def __init__(self):
        super().__init__("reset_node")
        self.create_subscription(
            Empty,
            "/reset_all",
            self.reset_all,
            5
        )
        self.reset_localization_pub = self.create_publisher(
            PoseWithCovarianceStamped,
            "/set_pose",
            5
        )
        self.reset_dvl_pub = self.create_publisher(
            ConfigCommand,
            "/dvl/config/command",
            5
        )
        self.reset_thrusts_pub = self.create_publisher(
            Empty,
            "/desired_thrust/reset",
            5
        )
        self.reset_thrust_cli = self.create_client(Empty, "/desired_thrust/reset")
        self.reset_pos_pubs : list[Publisher]= [None] * 6
        for (index, axis) in enumerate(['x', 'y', 'z', "roll", "pitch", "yaw"]):
            self.reset_pos_pubs[index] = self.create_client(Empty, f"/desired_position/{axis}/reset", 5)


    def reset_all(self, msg : Empty):
        self.reset_dvl_pub.publish(
            ConfigCommand(command="reset_dead_reckoning")
        )
        initial_pose = PoseWithCovarianceStamped()
        initial_pose.header.stamp = self.get_clock().now().to_msg()
        for i in [0, 7, 14, 21, 28, 35]:
            initial_pose.pose.covariance[i] = (10**-9)
        self.reset_localization_pub.publish(initial_pose)
        self.reset_thrusts_pub.publish(Empty())
        for reset_pos_pub in self.reset_pos_pubs:
            reset_pos_pub.publish(Empty())

def main():
    rclpy.init()
    reset_node = Reset()
    rclpy.spin(reset_node)

if __name__ == "__main__":
    main()

        