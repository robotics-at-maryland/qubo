#!/usr/bin/env python3
import rclpy
from rclpy.node import Node, Publisher, QoSProfile
from rclpy.qos import qos_profile_sensor_data
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
from scipy.spatial.transform import Rotation as R

class MinimalSubscriber(Node):

    def __init__(self):
        super().__init__('eul_pub')
        self.vectornav_sub = self.create_subscription(
            Imu,
            '/vectornav/imu',
            self.vectornav_callback,
            5)
        self.zed_sub = self.create_subscription(
            PoseWithCovarianceStamped,
            '/zed/zed_node/pose_with_covariance',
            self.zed_callback,
            5
        )
        self.dvl_sub = self.create_subscription(
            PoseWithCovarianceStamped,
            '/dvl/position',
            self.dvl_callback,
            qos_profile_sensor_data
        )
        self.localization_sub = self.create_subscription(
            Odometry,
            '/odometry/filtered',
            self.localization_callback,
            5
        )
        
        self.vectornav_rpy_pub : Publisher = self.create_publisher(Vector3, '/vectornav/rpy', 5)
        self.localization_rpy_pub : Publisher = self.create_publisher(Vector3, '/odometry/filtered/rpy', 5)
        self.zed_rpy_pub : Publisher = self.create_publisher(Vector3, '/zed/zed_node/rpy', 5)
        self.dvl_rpy_pub : Publisher = self.create_publisher(Vector3, '/dvl/rpy', 5)

    def vectornav_callback(self, msg : Imu):
        quat = msg.orientation
        rot = R.from_quat([quat.x, quat.y, quat.z, quat.w])
        eul = rot.as_euler('XYZ', degrees=True)
        
        rpyMessage = Vector3()
        rpyMessage.x = eul[0]
        rpyMessage.y = eul[1]
        rpyMessage.z = eul[2]
        self.vectornav_rpy_pub.publish(rpyMessage)

    def localization_callback(self, msg : Odometry):
        quat = msg.pose.pose.orientation
        rot = R.from_quat([quat.x, quat.y, quat.z, quat.w])
        eul = rot.as_euler('XYZ', degrees=True)
        
        rpyMessage = Vector3()
        rpyMessage.x = eul[0]
        rpyMessage.y = eul[1]
        rpyMessage.z = eul[2]
        self.localization_rpy_pub.publish(rpyMessage)
    
    def zed_callback(self, msg : PoseWithCovarianceStamped):
        quat = msg.pose.pose.orientation
        rot = R.from_quat([quat.x, quat.y, quat.z, quat.w])
        eul = rot.as_euler('XYZ', degrees=True)
        
        rpyMessage = Vector3()
        rpyMessage.x = eul[0]
        rpyMessage.y = eul[1]
        rpyMessage.z = eul[2]
        self.zed_rpy_pub.publish(rpyMessage)
    
    def dvl_callback(self, msg : PoseWithCovarianceStamped):
        quat = msg.pose.pose.orientation
        rot = R.from_quat([quat.x, quat.y, quat.z, quat.w])
        eul = rot.as_euler('XYZ', degrees=True)
        
        rpyMessage = Vector3()
        rpyMessage.x = eul[0]
        rpyMessage.y = eul[1]
        rpyMessage.z = eul[2]
        self.dvl_rpy_pub.publish(rpyMessage)

def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = MinimalSubscriber()

    rclpy.spin(minimal_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
