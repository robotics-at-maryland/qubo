#!/usr/bin/env python3
import math

from geometry_msgs.msg import TransformStamped, Vector3

import numpy as np

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Imu
from scipy.spatial.transform import Rotation as R

from tf2_ros import TransformBroadcaster

class IMUoffsetBroadcaster(Node): 
    def __init__(self): 
        super().__init__('vectornav_offset')
        self.vectornav_imu_sub = self.create_subscription(
            Imu, 
            '/vectornav/imu', 
            self.offset_callback, 
            5
        )
        self.vectornav_imu_pub = self.create_publisher(TransformStamped, '/vectornav/imu/offset', 5)
        self.vectornav_imu_pub_oe = self.create_publisher(Vector3, '/vectornav/imu/offset/euler', 5)
        self.vectornav_imu_pub_noe = self.create_publisher(Vector3, '/vectornav/imu/unoffset/euler', 5)
        self.vectornav_imu_sub_off = self.create_subscription(Vector3, '/vectornav/rpyoffset', self.add_offset_callback, 5)
        # self.tf_broadcaster = TransformBroadcaster(self)
        # self.subscription
        self.offset = Vector3()
        self.offset.x = 30.0
        self.offset.y = 30.0
        self.offset.z = 30.0
        
    
    def add_offset_callback(self, msg):
        self.offset.x += msg.x
        self.offset.y += msg.y
        self.offset.z += msg.z
    
    def offset_callback(self, msg):
        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = 'world'
        t.child_frame_id = 'vectornav_offset'

        t.transform.translation.x = 0.0
        t.transform.translation.y = 0.0
        t.transform.translation.z = 0.0

        e = R.from_quat([msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]).as_euler(seq = "XYZ", degrees=True)
        offseteuler = Vector3()
        offseteuler.x = e[0]
        offseteuler.y = e[1]
        offseteuler.z = e[2]
        self.vectornav_imu_pub_noe.publish(offseteuler)
        e[0] += self.offset.x
        e[1] += self.offset.y
        e[2] += self.offset.z
        noffseteuler = Vector3()
        noffseteuler.x = e[0]
        noffseteuler.y = e[1]
        noffseteuler.z = e[2]   
        self.vectornav_imu_pub_oe.publish(noffseteuler)
        q = R.from_euler('xyz', [e[0], e[1], e[2]], degrees=True)
        q2 = q.as_quat()
        t.transform.rotation.x = q2[0]
        t.transform.rotation.y = q2[1]
        t.transform.rotation.z = q2[2]
        t.transform.rotation.w = q2[3]
        # self.tf_broadcaster.sendTransform(t)
        self.vectornav_imu_pub.publish(t)
    
def main(args=None):
    rclpy.init(args=args)
    node = IMUoffsetBroadcaster()
    rclpy.spin(node)
    node.destroy_node()

    rclpy.shutdown()

if __name__ == '__main__':
    main()



        
        
