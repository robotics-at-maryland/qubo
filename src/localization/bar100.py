#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from geometry_msgs.msg import PoseWithCovarianceStamped

from kellerLD import KellerLD
import time

class DepthPublisher(Node):
    def __init__(self):
        super().__init__('depth_publisher_node')
        self.depth_publisher_ = self.create_publisher(PoseWithCovarianceStamped, 'depth', 10)
        self.water_temp_publisher_ = self.create_publisher(Float32, 'water_temp', 10)

        self.sensor = self.initialize_depth_sensor()
        self.offset = 1.30 # added to make 0 depth just breaking the surface
        timer_period = 0.2
        self.timer = self.create_timer(timer_period, self.timer_callback)
        
    def initialize_depth_sensor(self):
        sensor = KellerLD()
        if not sensor.init():
            print("Failed to initialize Keller LD sensor!")
            exit(1)
        time.sleep(3)
        if not sensor.read():
            print("Sensor read failed!")
            exit(1)
        return sensor
    
    def timer_callback(self):
        depth_msg = PoseWithCovarianceStamped()
        temp_msg = Float32()
        try:
            self.sensor.read()
        except:
            print("read failed")
            return

        # we negate depth to match our desired z axis
        depth_msg.header.frame_id = 'odom'
        depth_msg.header.stamp = self.get_clock().now().to_msg()
        depth_msg.pose.pose.position.z = -self.sensor.depth() + self.offset
        depth_msg.pose.covariance = [0.0] * 36
        depth_msg.pose.covariance[14] = .01

        temp_msg.data = self.sensor.temperature()

        self.depth_publisher_.publish(depth_msg)
        self.water_temp_publisher_.publish(temp_msg)

def main(args=None):
    rclpy.init(args=args)
    depth_publisher = DepthPublisher()
    rclpy.spin(depth_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    depth_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()