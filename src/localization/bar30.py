#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from std_msgs.msg import Float32

import ms5837


class DepthPublisher(Node):

    def __init__(self):
        super().__init__('depth_publisher')
        self.depth_publisher_ = self.create_publisher(Float32, 'depth', 10)
        self.water_temp_publisher_ = self.create_publisher(Float32, 'water_temp', 10)

        self.sensor = self.initialize_depth_sensor()
        self.offset = -0.05

        timer_period = 0.1  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def initialize_depth_sensor(self):
        sensor = ms5837.MS5837_30BA(1)
        if not sensor.init():
            print("Sensor could not be initialized")
            exit(1)

        if not sensor.read():
            print("Sensor read failed!")
            exit(1)

        sensor.setFluidDensity(1000)

        return sensor


    def timer_callback(self):
        depth_msg = Float32()
        temp_msg = Float32()
        try:
            self.sensor.read()
        except:
            print("error happened")
            return

        #we negate depth so its like z axis
        depth_msg.data = -self.sensor.depth() + self.offset 
        temp_msg.data = self.sensor.temperature()

        self.depth_publisher_.publish(depth_msg)
        self.water_temp_publisher_.publish(temp_msg)


def main(args=None):
    rclpy.init(args=args)

    depth_publisher = DepthPublisher()

    rclpy.spin(depth_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    depth_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()

