/* -*- C++ -*- */

// ROS includes
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/u_int8_multi_array.hpp"
#include "std_msgs/msg/bool.hpp"

#include "PCA9685.h"

using PneumaticsMsg = std_msgs::msg::UInt8MultiArray;
using std::placeholders::_1;

class PneumaticsSubscriber : public rclcpp::Node {
public:
    PneumaticsSubscriber();

private:
    void topic_callback(const PneumaticsMsg::SharedPtr msg);
    void marker_callback(const std_msgs::msg::Bool::SharedPtr msg);
    void claw_callback(const std_msgs::msg::Bool::SharedPtr msg);
    void torpedo_callback(const std_msgs::msg::Bool::SharedPtr msg);


    static int solenoid_map[];
    static const int MARKER_SOLENOID;
    static const int CLAW_SOLENOID;
    static const int TORPEDO_SOLENOID;
    PCA9685 &pwm_hat;
    rclcpp::Subscription<PneumaticsMsg>::SharedPtr subscription_;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr marker_subscription_;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr claw_subscription_;
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr torpedo_subscription_;
};