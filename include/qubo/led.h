/* -*- C++ -*- */

// ROS includes
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/u_int8_multi_array.hpp"

#include "PCA9685.h"

using LedMsg = std_msgs::msg::UInt8MultiArray;

using Int32 = std_msgs::msg::Int32;
using std::placeholders::_1;

class LedSubscriber : public rclcpp::Node {
public:
    LedSubscriber(PCA9685

private:
    void write_led_mode();
    void timer_callback();
    
    void topic_callback(const Int32::SharedPtr msg);

    int led_mode;
    bool const_on;
    bool curr_on;
    PCA9685 &pwm_hat;
    rclcpp::TimerBase::SharedPtr timer;
    rclcpp::Subscription<Int32>::SharedPtr subscription_;
};

