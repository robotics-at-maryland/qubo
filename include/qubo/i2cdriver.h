/* -*- C++ -*- */

#ifndef I2CDRIVER_H
#define I2CDRIVER_H

#include <cstdint>

extern "C" {
#include <mpsse.h>
}

#define DEBUG

#if defined(DEBUG)
#define LOG(x) do { std::cerr << x << '\n'; } while (0)
#else
#define LOG(x)
#endif

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using s32 = std::int32_t;

class I2CDevice {
public:
    I2CDevice(uint16_t paddr);
    virtual ~I2CDevice();
    int open_i2c(int num);
    int close_i2c();
    int write_byte(uint8_t data);
    int write_byte_reg(uint8_t reg, uint8_t data);
    int read_byte(uint8_t reg);
    int read_word(uint8_t cmd);

protected:
    //I2CDevice(uint16_t paddr);

    uint16_t addr;

private:
    static const int FT232H_VID;
    static const int FT232H_PID;
    static const int I2C_FREQ;

    static mpsse_context *context;
};

#endif
