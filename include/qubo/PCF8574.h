/* -*- C++ -*- */
/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
*/

#ifndef PCF8574_H
#define PCF8574_H

#include "i2cdriver.h"

class PCF8574 : protected I2CDevice {
public:
    static PCF8574 &get_instance();
    ~PCF8574();
    int write_io(u8 data);
    
private:
    PCF8574();
    static const int paddr;
    static const int bus_num;

    PCF8574(PCF8574 const &) = delete;
    void operator=(PCF8574 const &) = delete;
};

#endif
