/* -*- C++ -*- */
/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
*/

#ifndef PCA9685_H
#define PCA9685_H

#include "i2cdriver.h"

#define PCA9685_SLEEP 0x10

/* features this device should have
  - writing pwm to a specific channel
  - writing 0 to everything
*/

class PCA9685;

extern PCA9685 thruster_controller[];
extern PCA9685 pneumatics_controller;

class PCA9685 : protected I2CDevice {
public:
    static const int THRUSTER_BUS1;
    static const int THRUSTER_ADDR1;
    
    static const int THRUSTER_BUS2;
    static const int THRUSTER_ADDR2;
    
    static const int PNEUMATICS_BUS;
    static const int PNEUMATICS_ADDR;
    
    static const u8 PWM_LED[];

    PCA9685(const int bus, const int addr);
    static PCA9685 &get_instance();
    ~PCA9685();
    int write_pwm(const int pwm, const int channel);
    int write_on(const int channel);
    int write_off(const int channel);
    int write_full(const bool on, const int channel);
    
private:
    static const int pwm_freq_hz;
    static const int delay_percent;
    static const int PCA9685_ledn_prec;
    // static const int paddr;
    // static const int bus_num;

    //PCA9685();
    bool pwm_to_ledn(u8 *bytes, const int pwm) const;
    bool duty_to_ledn(u8 *bytes, const double percent) const;
    unsigned int duty_to_count(const double percent) const;

    //    PCA9685(PCA9685 const &) = delete;
    void operator=(PCA9685 const &) = delete;
};

#endif
