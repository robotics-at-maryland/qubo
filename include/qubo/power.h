/* -*- C++ -*- */

#ifndef POWER_H
#define POWER_H

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/battery_state.hpp"

#include "ADS7828.h"

using BatteryState = sensor_msgs::msg::BatteryState;

class PowerPublisher : public rclcpp::Node {
public:
    PowerPublisher(ADS7828 &ads);
    void publish_all();
    
private:
    void prepare_battery_info();
    void prepare_12v_info();
    void prepare_5v_info();
    void prepare_3_3v_info();
    void fill_simple_msg(BatteryState &msg, std::string loc,
                         float current, float voltage);

    ADS7828 &power_chip;
    rclcpp::TimerBase::SharedPtr timer;
    rclcpp::Publisher<BatteryState>::SharedPtr publisher;
};

#endif
