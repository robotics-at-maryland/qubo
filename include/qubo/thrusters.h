/* -*- C++ -*- */

#include "Eigen/Dense" //for linear algebra

// ROS includes
#include "rclcpp/rclcpp.hpp"
#include "rclcpp/timer.hpp"
#include "geometry_msgs/msg/wrench.hpp"
#include "geometry_msgs/msg/wrench_stamped.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/empty.hpp"
#include <unordered_map>
#include <string>
#include <chrono>
#include <cmath>

#include "PCA9685.h"

using PWMVector = Eigen::Matrix<double, 8, 1>;
using ForceVector = Eigen::Matrix<double, 6, 1>;
using Wrench = geometry_msgs::msg::Wrench;
using WrenchStamped = geometry_msgs::msg::WrenchStamped;
using String = std_msgs::msg::String;
using Empty = std_msgs::msg::Empty;
using std::placeholders::_1;

class ThrusterSubscriber : public rclcpp::Node {
public:
    ThrusterSubscriber();

private:
    void force_to_pwm(int voltage, PWMVector &pwm);
    void write_pwm(const PWMVector &pwm);
    void arm_thrust();
    void flip_pwm_vec(PWMVector &pwm);

    void add_desired_thrust_callback(const WrenchStamped::SharedPtr msg);
    void remove_desired_thrust_callback(const String::SharedPtr msg);
    void combine_desired_thrusts();
    void reset_desired_thrusts_callback(const Empty::SharedPtr msg);

    static const Eigen::Matrix<double, 8, 6> jetson_matrix;
    static int thruster_map_card1[], thruster_map_card2[];
    PCA9685 &pwm_hat1, &pwm_hat2;
    static const PWMVector zero_thrust;
    rclcpp::Subscription<WrenchStamped>::SharedPtr add_desired_thrust_sub;
    rclcpp::Subscription<String>::SharedPtr remove_desired_thrust_sub;
    rclcpp::TimerBase::SharedPtr combine_desired_thrusts_timer;
    rclcpp::Subscription<Empty>::SharedPtr reset_desired_thrusts_sub;
    std::unordered_map<std::string, std::pair<rclcpp::Time, ForceVector>> desired_thrusts;
};

