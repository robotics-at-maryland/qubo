/* -*- C++ -*- */
/* 2023 Robotics @ Maryland
   Alexander Yelovich, alexyel84@gmail.com
*/

#ifndef ADS7828_H
#define ADS7828_H

#include "i2cdriver.h"

class ADS7828 : protected I2CDevice {
public:
    ~ADS7828();
    static ADS7828 &get_instance();
    int read_channel(int channelno);
    float read_channel_voltage(int channelno);
    float read_channel_current(int channelno);

    static const int PRECISION_BITS;
    static const int PRECISION;
    static const float REF_VOLTAGE;

private:
    static const int paddr;
    static const int bus_num;

    ADS7828();

    ADS7828(ADS7828 const &) = delete;
    void operator=(ADS7828 const &) = delete;
};

#endif
